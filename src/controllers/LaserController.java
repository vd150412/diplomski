package controllers;

import main.Main;
import states.Game;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Line;
import objects.balls.Ball;
import java.util.List;

public class LaserController {

    private List<Ball> balls;
    private Line laserLine;

    public LaserController(double sunX, double sunY)
    {
        this.balls = Game.instance.balls;
        laserLine = new Line(sunX, sunY, sunX, sunY);
        Stop[] stops = new Stop []{
                new Stop(0, Color.RED),
                new Stop(0.5, Color.ORANGE),
                new Stop(1, Color.YELLOW)
        };

        LinearGradient lg = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
        laserLine.setStroke(lg);
        laserLine.setStrokeWidth(10);
        laserLine.getStrokeDashArray().addAll((double)20);
    }

    public void update()
    {
        double sunX = Game.instance.sun.getTranslateX();
        double sunY = Game.instance.sun.getTranslateY();

        double A = laserLine.getStartX();
        if(laserLine.getStartX() != laserLine.getEndX())
        {
            A = (laserLine.getStartY() - laserLine.getEndY()) / (laserLine.getStartX() - laserLine.getEndX());
        }
        //by = ax + c;
        double B = -1;
        double C = sunY - A * sunX;
        double minDist = Integer.MAX_VALUE;
        Ball ballFound = null;

        for(int i = 0; i < balls.size(); i++)
        {
            Ball ball = balls.get(i);
            double ballX = ball.getTranslateX();
            double ballY = ball.getTranslateY();

            if(intersects(A, B, C, ballX, ballY, Ball.BALL_RADIUS))
            {
                double lastX = Game.instance.sun.lastX;
                double lastY = Game.instance.sun.lastY;

                if(ballX > sunX && lastX > sunX || ballY > sunY && lastY > sunY ||
                        ballX < sunX && lastX < sunX || ballY < sunY && lastY < sunY)
                {
                    double dist = getDistance(sunX, sunY, ballX, ballY);
                    if(dist < minDist)
                    {
                        minDist = dist;
                        ballFound = ball;
                    }
                }
            }
        }

        for(int i = 0; i < balls.size(); i++)
        {
            Ball ball = balls.get(i);

            if(ball == ballFound)
            {
                ball.updateSelected(true);
                if(i - 1 >= 0) balls.get(i-1).updateSelected(true);
                if(i + 1 < balls.size()) balls.get(i + 1).updateSelected(true);
                //line.setEndY(ball.getTranslateY());
                //line.setEndX(ball.getTranslateX());
                i++;
            }
            else
            {
                ball.updateSelected(false);
            }
        }
    }

    private boolean intersects(double a, double b, double c, double x, double y, double r) {

        double dist = (Math.abs(a * x + b * y + c)) / Math.sqrt(a * a + b * b);

        if (r >= dist) return true;
        else return false;
    }

    private double getDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    //called from mouse event
    public void setLineEndPoint(double sunX, double sunY, double mouseX, double mouseY)
    {
        laserLine.setEndX(mouseX);
        laserLine.setEndY(mouseY);

        double A = laserLine.getStartX();
        if(laserLine.getStartX() != laserLine.getEndX())
        {
            A = (laserLine.getStartY() - laserLine.getEndY()) / (laserLine.getStartX() - laserLine.getEndX());
        }

        //y = ax + c;
        double C = sunY - A * sunX;

        if(Math.abs(A * Main.WINDOW_WIDTH / 2) <= Main.WINDOW_HEIGHT / 2)
        {
            if(mouseX > sunX)
            {
                laserLine.setEndX(Main.WINDOW_WIDTH);
                laserLine.setEndY(A * Main.WINDOW_WIDTH + C);
            }
            else if(mouseX < sunX)
            {
                laserLine.setEndX(0);
                laserLine.setEndY(C);
            }
        }
        else
        {
            if(mouseY > sunY)
            {
                laserLine.setEndY(Main.WINDOW_HEIGHT);
                laserLine.setEndX((laserLine.getEndY() - C) / A);
            }
            else if(mouseY < sunY)
            {
                laserLine.setEndY(0);
                laserLine.setEndX(-C/A);
            }
        }
    }

    public Line getLaserLine() {
        return laserLine;
    }

    public void disableLine() {
        laserLine.setEndX(laserLine.getStartX());
        laserLine.setEndY(laserLine.getStartY());
    }
}
