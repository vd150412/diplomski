package controllers;

import javafx.scene.shape.Line;
import main.Main;
import objects.Sun;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.*;
import java.net.URLDecoder;
import java.util.ArrayList;

public class Config {

    public static class SettingsItem {
        public String id;
        public String settingsText;
        public double value;
        public double minValue;
        public double maxValue;

        public SettingsItem(String id, String settingsText, double value, double minValue, double maxValue) {
            this.id = id;
            this.settingsText = settingsText;
            this.value = value;
            this.minValue = minValue;
            this.maxValue = maxValue;
        }
    }

    public static double BALL_SPEED = 1.5;
    public static double MAX_BALLS = 11;

    public static double COIN_FREQ = 0.9;
    public static double COIN_PICK_ACTIVE = 4;

    public static double LASER_FREQ = 0.1;
    public static double LASER_PICK_ACTIVE = 4;
    public static double LASER_DURATION = 4;

    public static double BALL_REVERSE_FREQ = 0.03;
    public static double BALL_REVERSE_DISTANCE = 8;

    public static double BALL_PAUSE_FREQ = 0.03;
    public static double BALL_PAUSE_DURATION = 4;

    public static double BALL_SHIELD_FREQ = 0.03;

    public static double BALL_BOMB_FREQ = 0.02;
    public static double BALL_BOMB_TO_DESTROY = 2;

    public static double RAND_BALL_REVERSE = 1 - BALL_REVERSE_FREQ;
    public static double RAND_BALL_PAUSE = RAND_BALL_REVERSE - BALL_PAUSE_FREQ;
    public static double RAND_BALL_SHIELD = RAND_BALL_PAUSE - BALL_SHIELD_FREQ;
    public static double RAND_BALL_BOMB = RAND_BALL_SHIELD - BALL_BOMB_FREQ;

    public static String LEVEL_PATH = "0.067499995 0.28142858 0.10750001 0.18714286 0.17916666 0.12142857 0.30166668 0.105714284 0.58916664 0.11285714 0.7475 0.15571427 0.86 0.25285715 0.90666664 0.40142858 0.91083336 0.69285715 0.85583335 0.82 0.7316667 0.8757143 0.4175 0.87 0.17583334 0.87 0.10166666 0.75857145 0.093333334 0.6357143 0.10500001 0.42857143 0.16166668 0.3157143 0.2925 0.26 0.42416665 0.29 0.4975 0.37 0.5175 0.49857143 0.5183333 0.61142856 0.49583334 0.70714283 0.4375 0.77 0.35833332 0.7757143 0.3 0.76285714 0.26749998 0.70714283 0.26333332 0.63285714 0.26333332 0.54285717";
    public static String LEVEL_SUN_POSITION = "0.68333334 0.47714287";
    public static final String JSON_PATH_FIELD = "PATH";
    public static final String JSON_SUN_POSITION = "SUN_POSITION";

    public static String CONFIG_FILE_PATH = "config.json";
    public static String CONFIG_FILE_NAME = "config.json";
    public static File FILE_CHOOSER_DIR = null;

    public static final SettingsItem[] SETTINGS_ITEMS = {
            new SettingsItem("BALL_SPEED", "Brzina pokretnih kugli", Config.BALL_SPEED, 1, 5),
            new SettingsItem("MAX_BALLS","Ukupan broj pokretnih kugli", Config.MAX_BALLS, 10, 400),

            new SettingsItem("COIN_FREQ","Frekvencija generisanja novcica na obodu ekrana", Config.COIN_FREQ, 0, 1),
            new SettingsItem("COIN_PICK_TICKS_ACTIVE","Vreme zadrzavanja novcica na obodu ekrana", Config.COIN_PICK_ACTIVE, 2, 10),

            new SettingsItem("LASER_FREQ","Frekvencija generisanja laserskog topa na obodu ekrana", Config.LASER_FREQ, 0, 1),
            new SettingsItem("LASER_PICK_TICKS_ACTIVE","Vreme zadrzavanja laserskog topa na obodu ekrana", Config.LASER_PICK_ACTIVE, 2, 10),
            new SettingsItem("LASER_DURATION","Vreme trajanja osvojenog laserskog topa", Config.LASER_DURATION, 1, 10),

            new SettingsItem("BALL_REVERSE_FREQ","Verovatnoca pojavljivanja kugle koja izaziva vracanje kugli unazad", Config.BALL_REVERSE_FREQ, 0, 1),
            new SettingsItem("BALL_REVERSE_DISTANCE","Broj mesta za koji ce se kugle pomeriti unazad", Config.BALL_REVERSE_DISTANCE, 1, 15),

            new SettingsItem("BALL_PAUSE_FREQ","Veroratnoca pojavljivanja kugle koja zaustavlja napredovanje ostalih kugli", Config.BALL_PAUSE_FREQ, 0, 1),
            new SettingsItem("BALL_PAUSE_TICKS_TO_PAUSE","Vreme zaustavljanja napredovanja ostalih kugli", Config.BALL_PAUSE_DURATION, 1, 10),

            new SettingsItem("BALL_SHIELD_FREQ","Verovatnoca pojavljivanja kugle sa stitom", Config.BALL_SHIELD_FREQ, 0, 1),

            new SettingsItem("BALL_BOMB_FREQ","Verovatnoca pojavljivanja kugle bombe", Config.BALL_BOMB_FREQ, 0, 1),
            new SettingsItem("BALL_BOMB_TO_DESTROY","Broj susednih kugli koje ce unistiti kugla bomba", Config.BALL_BOMB_TO_DESTROY, 1, 5),
    };

    public static final String TOP_LIST_ENTRY = "TOP_LIST_";
    public static ArrayList<TopListController.TopListEntry> TOP_LIST = new ArrayList<>();

    private static JSONObject JSON_OBJECT;

    public static boolean LOAD_JSON(File selectedFile)
    {
        JSONParser jsonParser = new JSONParser();
        CONFIG_FILE_PATH = selectedFile.getAbsolutePath();
        CONFIG_FILE_NAME = selectedFile.getName();
        System.out.println("path: " + CONFIG_FILE_PATH + ", name: " + CONFIG_FILE_NAME);
        boolean error = false;
        try
        {
            Object obj = jsonParser.parse(new FileReader(selectedFile));
            JSONObject jo = (JSONObject) obj;
            JSON_OBJECT = jo;

            Config.LEVEL_PATH = jo.get(JSON_PATH_FIELD).toString();
            Config.LEVEL_SUN_POSITION = jo.get(JSON_SUN_POSITION).toString();

            TOP_LIST = new ArrayList<>();
            for(int i = 0; i < 10; i++)
            {
                Object entry = jo.get(TOP_LIST_ENTRY + i);
                if (entry == null) break;
                String entryStr = entry.toString();

                String[] arr = entryStr.split("#");
                TopListController.TopListEntry topListEntry = new TopListController.TopListEntry(arr[0], Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));
                TOP_LIST.add(topListEntry);
            }
            if(Main.instance.topListController != null)
            {
                Main.instance.topListController.setTopList(TOP_LIST);
            }

            for(SettingsItem item : SETTINGS_ITEMS)
            {
                Object valStr = jo.get(item.id);
                double val = Double.parseDouble((String)valStr);
                if(val > item.maxValue || val < item.minValue)
                {
                    error = true;
                    break;
                }
                item.value = val;
            }

            Config.BALL_SPEED = SETTINGS_ITEMS[0].value;
            Config.MAX_BALLS = SETTINGS_ITEMS[1].value;

            Config.COIN_FREQ = SETTINGS_ITEMS[2].value;
            Config.COIN_PICK_ACTIVE = SETTINGS_ITEMS[3].value;

            Config.LASER_FREQ = SETTINGS_ITEMS[4].value;
            Config.LASER_PICK_ACTIVE = SETTINGS_ITEMS[5].value;
            Config.LASER_DURATION = SETTINGS_ITEMS[6].value;

            Config.BALL_REVERSE_FREQ = SETTINGS_ITEMS[7].value;
            Config.BALL_REVERSE_DISTANCE = SETTINGS_ITEMS[8].value;

            Config.BALL_PAUSE_FREQ = SETTINGS_ITEMS[9].value;
            Config.BALL_PAUSE_DURATION = SETTINGS_ITEMS[10].value;

            Config.BALL_SHIELD_FREQ = SETTINGS_ITEMS[11].value;

            Config.BALL_BOMB_FREQ = SETTINGS_ITEMS[12].value;
            Config.BALL_BOMB_TO_DESTROY = SETTINGS_ITEMS[13].value;

            Config.RAND_BALL_REVERSE = 1 - Config.BALL_REVERSE_FREQ;
            Config.RAND_BALL_PAUSE = Config.RAND_BALL_REVERSE - Config.BALL_PAUSE_FREQ;
            Config.RAND_BALL_SHIELD = Config.RAND_BALL_PAUSE - Config.BALL_SHIELD_FREQ;
            Config.RAND_BALL_BOMB = Config.RAND_BALL_SHIELD - Config.BALL_BOMB_FREQ;
        }
        catch (Exception e)
        {
            error = true;
            e.printStackTrace();
        }

        return error;
    }

    public static JSONObject GET_JSON_OBJECT(File selectedFile)
    {
        JSONParser jsonParser = new JSONParser();
        try
        {
            Object obj = jsonParser.parse(new FileReader(selectedFile));
            JSONObject jo = (JSONObject) obj;
            return jo;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static void SAVE_JSON(ArrayList<TopListController.TopListEntry> topList)
    {
        JSONWriter.SAVE_JSON(topList);
    }

    public static boolean SAVE_JSON(File file, ArrayList<Line> lines, Sun sun)
    {
        String pathStr = getPathStr(lines);
        String sunStr = getSunStr(sun);

        if(file.exists())
        {
            return JSONWriter.SAVE_JSON(file, pathStr, sunStr, GET_JSON_OBJECT(file));
        }
        else
        {
            return JSONWriter.SAVE_JSON(file, pathStr, sunStr, null);
        }
    }

    public static String getPathStr(ArrayList<Line> lines) {
        String path = "";
        for(Line line : lines)
        {
            float percentageX = (float) (line.getStartX() / Main.WINDOW_WIDTH);
            float percentageY = (float) (line.getStartY() / Main.WINDOW_HEIGHT);
            path += percentageX + " " + percentageY + " ";
        }

        Line lastLine = lines.get(lines.size() - 1);
        float percentageX = (float) (lastLine.getEndX() / Main.WINDOW_WIDTH);
        float percentageY = (float) (lastLine.getEndY() / Main.WINDOW_HEIGHT);
        path += percentageX + " " + percentageY + " ";

        return path;
    }

    public static String getSunStr(Sun sun) {
        //sun
        String sunPosition = "";
        float percentageSunX = (float) (sun.getTranslateX() / Main.WINDOW_WIDTH);
        float percentageSunY = (float) (sun.getTranslateY() / Main.WINDOW_HEIGHT);
        sunPosition += percentageSunX + " " + percentageSunY;

        return sunPosition;
    }

    public static File getFileChooserDir() {

        if(FILE_CHOOSER_DIR == null)
        {
            String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            try
            {
                String decodedPath = URLDecoder.decode(path, "UTF-8");
                FILE_CHOOSER_DIR = new File(decodedPath).getParentFile();
            }
            catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
                FILE_CHOOSER_DIR = new File("/C:/");
            }
        }

        return FILE_CHOOSER_DIR;
    }

    public static int FINAL_SCORE() {
        int score = Main.instance.game.textScore.getScore();
        //int time = Main.instance.game.textTime.getTime();
        //int toAdd = (int) (Config.MAX_BALLS * 2 - time) / 2;
        //score += toAdd;
        return score;
    }

}
