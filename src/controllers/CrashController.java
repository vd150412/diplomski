package controllers;

import main.Main;
import objects.balls.Ball;
import objects.balls.BallBomb;
import objects.balls.BallShield;
import objects.balls.Shot;
import states.*;
import states.GameState;
import javafx.scene.Group;
import text.TextScore;

import java.util.List;

import static main.Main.WINDOW_HEIGHT;
import static main.Main.WINDOW_WIDTH;

public class CrashController {

    private List<Ball> balls;
    private List<Shot> shots;
    private Group root;
    private BallController ballController;
    private TextScore textScore;

    public CrashController()
    {
        this.balls = Game.instance.balls;
        this.shots = Game.instance.shots;
        this.root = Game.instance.root;
        this.ballController = Game.instance.ballController;
        this.textScore = Game.instance.textScore;
    }

    public void update()
    {
        for (int i = 0; i < shots.size(); i++) {
            Shot shot = shots.get(i);
            shot.update();
            if (shot.getTranslateX() < 0 || shot.getTranslateX() > WINDOW_WIDTH
                    || shot.getTranslateY() < 0 || shot.getTranslateY() > WINDOW_HEIGHT) {
                root.getChildren().remove(shot);
                shots.remove(shot);
                Game.instance.shotMissController.shotMiss();
            } else {
                for (int j = 0; j < balls.size(); j++) {
                    if (shot.getBoundsInParent().intersects(balls.get(j).getBoundsInParent())) {

                        if(Main.instance.gameState == GameState.LASER_BONUS)
                        {
                            laserCrashLogic(shot, j);
                            Game.instance.shotMissController.shotHit();
                            break;
                        }
                        else
                        {
                            crashLogic(shot, j);
                            break;
                        }
                    }
                }
            }
        }
    }

    public void laserCrashLogic(Shot shot, int j)
    {
        Game.instance.laserPickController.triggerLaser();
        shots.remove(shot);
        root.getChildren().remove(shot);

        int scoreToAdd = 0;
        if(j < balls.size()) j++;

        for(int NUM = 0; NUM < 3; NUM++)
        {
            if(balls.size() > 0 && j >= 0 && j < balls.size())
            {
                scoreToAdd += balls.get(j).calcScore();
                root.getChildren().remove(balls.get(j));
                balls.remove(j);
            }

            j--;
        }

        for (int m = j; m >= 0; m--) {
            balls.get(m).reversePath(3);
        }
        textScore.updateScore(scoreToAdd);
    }

    public void crashLogic(Shot shot, int j) {

        Ball hitBall = balls.get(j);
        Ball nextPath = (j - 1 >= 0) ? balls.get(j - 1) : null;
        Ball prevPath = (j + 1 < balls.size()) ? balls.get(j + 1) : null;

        BallShield ballShield = shieldHit(shot, hitBall, nextPath, prevPath);

        if(ballShield != null) ballShield.removeShield();

        if(ballShield == null && prevPath != null && shot.getColor().equals(prevPath.getColor()))
        {
            j++;
            hitBall = prevPath;
            nextPath = (j - 1 >= 0) ? balls.get(j - 1) : null;
            prevPath = (j + 1 < balls.size()) ? balls.get(j + 1) : null;
        }

        if (ballShield == null && shot.getColor().equals(hitBall.getColor())
                && ((nextPath != null && nextPath.canRemove() && shot.getColor().equals(nextPath.getColor()))
                || (prevPath != null && prevPath.canRemove() && shot.getColor().equals(prevPath.getColor())))) {
            root.getChildren().remove(shot);
            int sameColorCnt = 0, k;
            int scoreToAdd = 0;
            boolean foundBomb = false;

            //next
            for (k = j + 1; k < balls.size() && balls.get(k).canRemove() && balls.get(k).getColor().equals(shot.getColor()); sameColorCnt++) {
                root.getChildren().remove(balls.get(k));
                scoreToAdd += balls.get(k).calcScore();

                ballController.changeState(balls.get(k));
                if(balls.get(k) instanceof BallBomb) foundBomb = true;

                balls.remove(k);
            }
            //previous
            for (k = j; k >= 0 && balls.get(k).canRemove() && balls.get(k).getColor().equals(shot.getColor()); k--, sameColorCnt++) {
                root.getChildren().remove(balls.get(k));
                scoreToAdd += balls.get(k).calcScore();

                ballController.changeState(balls.get(k));
                if(balls.get(k) instanceof BallBomb) foundBomb = true;

                balls.remove(k);
            }

            if(foundBomb == true)
            {
                int ballsRemoved = 0;
                k++;
                while(ballsRemoved < Config.BALL_BOMB_TO_DESTROY && k < balls.size())
                {
                    root.getChildren().remove(balls.get(k));
                    scoreToAdd += balls.get(k).calcScore();
                    balls.remove(k);

                    ballsRemoved++;
                    sameColorCnt++;
                }
                k--;

                ballsRemoved = 0;
                while(ballsRemoved < Config.BALL_BOMB_TO_DESTROY && k >= 0 && balls.size() > 0)
                {
                    root.getChildren().remove(balls.get(k));
                    scoreToAdd += balls.get(k).calcScore();
                    balls.remove(k);

                    ballsRemoved++;
                    sameColorCnt++;
                    k--;

                }
            }

            //reverse
            for (int m = k; m >= 0; m--) {
                balls.get(m).reversePath(sameColorCnt);
            }

            textScore.updateScore(scoreToAdd);
            Game.instance.shotMissController.shotHit();
        }
        else
        {
            if(prevPath != null && nextPath != null)
            {
                shot.becomeMoving(prevPath);
                balls.add(j + 1, shot);
                for(int k = 0; k < j + 2; k++)
                {
                    Ball hit = balls.get(k);
                    hit.reversePath(-1);
                }
            }
            else if(prevPath == null)
            {
                Ball lastBall = balls.get(balls.size() - 1);
                shot.becomeMoving(lastBall);
                balls.add(shot);
                shot.reversePath(1);
            }
            else if(nextPath == null)
            {
                Ball firstBall = balls.get(0);
                shot.becomeMoving(firstBall);
                balls.add(1, shot);
                firstBall.reversePath(-1);
            }

            Game.instance.shotMissController.shotMiss();
        }
        shots.remove(shot);
    }

    private BallShield shieldHit(Shot shot, Ball... balls)
    {
        for(Ball ball : balls)
        {
            if (ball instanceof BallShield && !ball.canRemove() &&
                    ball.getColor().equals(shot.getColor()) &&
                    shot.getBoundsInParent().intersects(ball.getBoundsInParent()))
            {
                return (BallShield) ball;
            }
        }

        return null;
    }
}
