package controllers;


import main.Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class TopListController {

    public static class TopListEntry {
        public String name;
        public int score;
        public int time;

        public TopListEntry(String name, int score, int time) {
            this.name = name;
            this.score = score;
            this.time = time;
        }
    }

    //player_name - score
    private ArrayList<TopListEntry> topList = new ArrayList<>();

    public TopListController()
    {
        topList = Config.TOP_LIST;
    }

    public ArrayList<TopListEntry> getTopList() {
        return topList;
    }

    public void setTopList(ArrayList<TopListEntry> topList) {
        this.topList = topList;
    }

    public boolean isInTop10()
    {
        if(topList.size() < 10) return true;

        TopListEntry lastEntry = topList.get(topList.size() - 1);

        if(lastEntry.score < Config.FINAL_SCORE()) return true;

        if(lastEntry.score == Config.FINAL_SCORE())
        {
            int time = Main.instance.game.textTime.getTime();
            if(time < lastEntry.time) return true;
        }

        return false;
    }

    public int onNameEntered(String name, int score, int time)
    {
        int scoreIndex = -1;

        for(int i = 0; i < topList.size(); i++)
        {
            if(score > topList.get(i).score ||
                    (score == topList.get(i).score && time < topList.get(i).time))
            {
                topList.add(i,new TopListEntry(name, score, time));
                if(topList.size() > 10)
                {
                    topList.remove(topList.size() - 1);
                }
                scoreIndex = i;
                break;
            }
        }

        if(scoreIndex == -1 && topList.size() < 10)
        {
            scoreIndex = topList.size();
            topList.add(new TopListEntry(name, score, time));
        }

        if(scoreIndex != -1)
        {
            Config.SAVE_JSON(topList);
        }

        return scoreIndex;
    }
}
