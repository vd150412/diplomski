package controllers;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineJoin;
import javafx.util.Pair;
import main.Main;
import objects.balls.Ball;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class LevelController {

    public static LevelController instance;
    private ArrayList<Line> lines;
    private ArrayList<Double> distances;
    private ArrayList<Pair<Float, Float>> path = new ArrayList<>();

    public static int FIRST_INDEX = (int) Math.round(Ball.BALL_RADIUS * 2 / Config.BALL_SPEED);
    public double sunX = Main.WINDOW_WIDTH / 2, sunY = Main.WINDOW_HEIGHT / 2;

    public LevelController(Group root)
    {
        instance = this;
        lines = new ArrayList<>();
        distances = new ArrayList<>();
        FIRST_INDEX = (int) Math.round(Ball.BALL_RADIUS * 2 / Config.BALL_SPEED);
        generateLines(Config.LEVEL_PATH, root);
        setSunPosition(Config.LEVEL_SUN_POSITION);
        generatePath();
    }

    public LevelController(Group root, ArrayList<Line> lines, double sunX, double sunY)
    {
        instance = this;

        this.lines = new ArrayList<>();
        this.distances = new ArrayList<>();
        this.FIRST_INDEX = (int) Math.round(Ball.BALL_RADIUS * 2 / Config.BALL_SPEED);

        this.sunX = sunX;
        this.sunY = sunY;

        for(Line line : lines)
        {
            if(getDistance(line) > 0)
            {
                this.lines.add(line);
                distances.add(getDistance(line));
            }
        }

        initPolygon(root);
        generatePath();
    }

    private void setSunPosition(String lineStr)
    {
        String[] linesStr = lineStr.split(" ");
        sunX = Float.parseFloat(linesStr[0]) * Main.WINDOW_WIDTH;
        sunY = Float.parseFloat(linesStr[1]) * Main.WINDOW_HEIGHT;
    }

    private void generateLines(String lineStr, Group root)
    {
        double lastX = 0, lastY = 0;
        String[] linesStr = lineStr.split(" ");

        for(int i = 0; i < linesStr.length - 1; i += 2)
        {
            double x = Float.parseFloat(linesStr[i]) * Main.WINDOW_WIDTH;
            double y = Float.parseFloat(linesStr[i + 1]) * Main.WINDOW_HEIGHT;

            if(i != 0)
            {
                Line lineShape = new Line();
                lineShape.setStartX(lastX);
                lineShape.setStartY(lastY);
                lineShape.setEndX(x);
                lineShape.setEndY(y);

                if(getDistance(lineShape) > 0)
                {
                    lines.add(lineShape);
                    distances.add(getDistance(lineShape));
                }
            }

            lastX = x;
            lastY = y;
        }

        initPolygon(root);
    }

    private void initPolygon(Group root)
    {
        Polygon polygon = new Polygon();
        polygon.setStrokeWidth(25);
        polygon.setStroke(Color.ORANGE);
        Polygon polygon2 = new Polygon();
        polygon2.setStrokeWidth(20);
        polygon2.setStroke(Color.YELLOW);

        polygon.setStrokeLineJoin(StrokeLineJoin.ROUND);
        polygon2.setStrokeLineJoin(StrokeLineJoin.ROUND);

        for(Line line : lines)
        {
            polygon.getPoints().addAll(line.getStartX(), line.getStartY());
            polygon2.getPoints().addAll(line.getStartX(), line.getStartY());
        }

        Line lastLine = lines.get(lines.size() - 1);
        polygon.getPoints().addAll(lastLine.getEndX(), lastLine.getEndY());
        polygon2.getPoints().addAll(lastLine.getEndX(), lastLine.getEndY());

        for(int i = lines.size() - 1; i >= 0; i--)
        {
            Line line = lines.get(i);
            polygon.getPoints().addAll(line.getStartX(), line.getStartY());
            polygon2.getPoints().addAll(line.getStartX(), line.getStartY());
        }

        root.getChildren().addAll(polygon, polygon2);
    }

    private void generatePath()
    {
        double distanceX = 0, distanceY = 0;
        Ball ball = new Ball();
        for(int i = 0; i < lines.size(); i++)
        {
            double velX = getVelocityX(i);
            double velY = getVelocityY(i);
            ball.setTranslateX(getX(i));
            ball.setTranslateY(getY(i));
            double distanceToPass =  distances.get(i);

            while(true)
            {
                float x = (float) (ball.getTranslateX() + velX);
                float y = (float) (ball.getTranslateY() - velY);
                path.add(new Pair<>(x, y));
                ball.setTranslateX(x);
                ball.setTranslateY(y);
                distanceX += Math.abs(velX);
                distanceY += Math.abs(velY);

                if(distanceX > distanceToPass || distanceY > distanceToPass)
                {
                    distanceX = 0;
                    distanceY = 0;

                    if(i + 1 >= lines.size()) break;

                    ball.setTranslateX(getX(i + 1));
                    ball.setTranslateY(getY(i + 1));

                    break;
                }
            }
        }
    }

    private double getDistance(Line line) {
        double distanceX = Math.abs(line.getEndX() - line.getStartX());
        double distanceY = Math.abs(line.getEndY() - line.getStartY());
        return Math.max(distanceX, distanceY);
    }

    public double getVelocityX(int lineIndex) {
        Line line = lines.get(lineIndex);
        double distanceX = line.getEndX() - line.getStartX();
        double distanceY = line.getStartY() - line.getEndY();
        double distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        return distanceX / distance * Config.BALL_SPEED;
    }

    public double getVelocityY(int lineIndex) {
        Line line = lines.get(lineIndex);
        double distanceX = line.getEndX() - line.getStartX();
        double distanceY = line.getStartY() - line.getEndY();
        double distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        return distanceY / distance * Config.BALL_SPEED;
    }

    public double getX(int lineIndex) {
        return lines.get(lineIndex).getStartX();
    }

    public double getY(int lineIndex) {
        return lines.get(lineIndex).getStartY();
    }

    public Line getLastLine() {
        return lines.get(lines.size() - 1);
    }

    public Pair<Float, Float> getPath(int index)
    {
        if(index >= 0)
        {
            return path.get(index);
        }
        else
        {
            Pair<Float, Float> first = path.get(0);

            double velX = getVelocityX(0);
            double velY = getVelocityY(0);

            float x = (float) (first.getKey() + index * velX);
            float y = (float) (first.getValue() - index * velY);

            Pair<Float, Float> pathPrev = new Pair<>(x, y);
            return pathPrev;
        }
    }

    public int reverseIndex(int index, int times) {
        return index - times * FIRST_INDEX;
    }

}
