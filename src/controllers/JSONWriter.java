package controllers;


import org.json.JSONObject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static controllers.Config.*;

public class JSONWriter {


    public static boolean SAVE_JSON(File selectedFile, String path, String sunPosition, org.json.simple.JSONObject config)
    {
        boolean errors = false;

        JSONObject jo = new JSONObject();

        for(Config.SettingsItem item : SETTINGS_ITEMS)
        {
            if(config != null)
            {
                //called when modifying existing config file
                jo.put(item.id, "" + config.get(item.id) + "");
            }
            else
            {
                //called when new config file is created
                jo.put(item.id, "" + item.value + "");
            }
        }

        jo.put(JSON_PATH_FIELD, path);
        jo.put(JSON_SUN_POSITION, sunPosition);

        try (FileWriter file = new FileWriter(selectedFile))
        {
            file.write(jo.toString(4));
        }
        catch (IOException e)
        {
            errors = true;
            e.printStackTrace();
        }

        return errors;
    }

    public static void SAVE_JSON(ArrayList<TopListController.TopListEntry> topList)
    {
        JSONObject jo = new JSONObject();

        for(Config.SettingsItem item : SETTINGS_ITEMS)
        {
            jo.put(item.id, "" + item.value + "");
        }

        jo.put(JSON_PATH_FIELD, LEVEL_PATH);
        jo.put(JSON_SUN_POSITION, LEVEL_SUN_POSITION);

        for(int i = 0; i < topList.size(); i++)
        {
            TopListController.TopListEntry entry = topList.get(i);
            String str = entry.name + "#" + entry.score + "#" + entry.time;
            jo.put(TOP_LIST_ENTRY + i, str);
        }

        try (FileWriter file = new FileWriter(CONFIG_FILE_PATH)) {
            file.write(jo.toString(4));;
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
