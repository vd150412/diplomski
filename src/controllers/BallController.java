package controllers;

import objects.balls.Ball;
import objects.balls.BallPause;
import objects.balls.BallReverse;

import java.util.List;

public class BallController {

    public static BallController instance;
    private static final int VELOCITY_NORMAL = 1;
    private List<Ball> balls;
    private BallAction currentAction;

    public interface BallAction {
        void update();
        void startAction();
        int getBallVelocity();
    }

    BallAction ballReverseAction = new BallAction() {

        private final int REVERSE_PATH = (int) (LevelController.FIRST_INDEX * Config.BALL_REVERSE_DISTANCE);
        private final int VELOCITY_REVERSE = -8;
        private int reverseDistance;

        @Override
        public void startAction() {
            reverseDistance = 0;
            for(Ball ball : balls)
            {
                ball.setVelocity(VELOCITY_REVERSE);
            }
        }

        @Override
        public void update() {
            reverseDistance += VELOCITY_REVERSE * -1;
            if(reverseDistance > REVERSE_PATH)
            {
                changeAction(ballNormalAction);
            }
        }

        @Override
        public int getBallVelocity() {
            return VELOCITY_REVERSE;
        }
    };

    BallAction ballPauseAction = new BallAction() {

        private final int TICKS_TO_PAUSE = (int) (Config.BALL_PAUSE_DURATION * 60);
        private int pauseTicks = 0;

        @Override
        public void startAction() {
            pauseTicks = TICKS_TO_PAUSE;
            for(Ball ball : balls)
            {
                ball.setVelocity(0);
            }
        }

        @Override
        public void update() {
            if(pauseTicks-- <= 0)
            {
                changeAction(ballNormalAction);
            }
        }

        @Override
        public int getBallVelocity() {
            return 0;
        }
    };

    BallAction ballNormalAction = new BallAction() {

        @Override
        public void update() {

        }

        @Override
        public void startAction() {
            for(Ball ball : balls)
            {
                ball.setVelocity(VELOCITY_NORMAL);
            }
        }

        @Override
        public int getBallVelocity() {
            return VELOCITY_NORMAL;
        }
    };

    public BallController(List<Ball> balls) {
        this.balls = balls;
        instance = this;
        currentAction = ballNormalAction;
    }

    public void update() {
        currentAction.update();
    }

    public int getBallVelocity() {
        return currentAction.getBallVelocity();
    }

    public void changeState(Ball ball)
    {
        if(ball instanceof BallPause) changeAction(ballPauseAction);
        else if(ball instanceof BallReverse) changeAction(ballReverseAction);
    }

    public void changeAction(BallAction action) {
        this.currentAction = action;
        currentAction.startAction();
    }

}
