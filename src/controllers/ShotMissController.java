package controllers;

import states.Game;
import objects.balls.Ball;
import objects.balls.Shot;

public class ShotMissController {

    public static final int MISSED_SHOTS_LIMIT = 10;
    public static final int TICKS_BALLS_BLACK = 60 * 5;

    private int missedShots = 0;
    private int ticksToWait = 0;
    private boolean ballsBlack = false;

    public void update()
    {
        if(ballsBlack == false) return;

        if(--ticksToWait == 0)
        {
            ballsBlack = false;
            for(Ball ball : Game.instance.balls)
            {
                ball.fromBlack();
            }

            for(Shot shot : Game.instance.shots) {
                shot.fromBlack();
            }

            Game.instance.sun.removeClouds();
        }
    }

    public void shotMiss()
    {
        if(++missedShots >= MISSED_SHOTS_LIMIT)
        {
            missedShots = 0;
            ticksToWait = TICKS_BALLS_BLACK;

            if(ballsBlack == false)
            {
                for(Ball ball : Game.instance.balls) {
                    ball.toBlack();
                }

                for(Shot shot : Game.instance.shots) {
                    shot.toBlack();
                }

                Game.instance.sun.addClouds();
            }

            ballsBlack = true;
        }
    }

    public void shotHit() {
        missedShots = 0;
    }

    public boolean ballsBlack() {
        return ballsBlack;
    }
}
