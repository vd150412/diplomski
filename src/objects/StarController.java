/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import java.util.Random;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import main.Main;
import objects.Star;

/**
 *
 * @author Korisnik
 */
public class StarController {
    
    public static final int MIN_STARS = 3;
    public static final int MAX_STARS = 10;
    public static final int TICKS_TO_ACTIVATE = (int) (60 * 60 / 7.0);
    
    //w/4.5, h/3.8
    public static final double RX_MIN = Main.WINDOW_WIDTH / 5.5;
    public static final double RX_MAX = Main.WINDOW_WIDTH / 4;
    public static final double RY_MIN = Main.WINDOW_HEIGHT / 4.2 - 15;
    public static final double RY_MAX = Main.WINDOW_HEIGHT / 3.2 - 15; 
    
    private int currentStars;   
    private int currentTicks = 0;
    private Group root;
    private Random rand;
    
    public StarController(Group root) 
    {
        this.root = root;
        this.rand = new Random();
        
        for(int i = 0; i < MIN_STARS; i++)
        {
            createStar();
        } 
    }
    
    public void update()
    {
        if(++currentTicks == TICKS_TO_ACTIVATE && currentStars < MAX_STARS)
        {
            currentTicks = 0;
            createStar();
        }
    }
    
    private void createStar()
    {
        double randNum = rand.nextDouble();
        double rX = RX_MIN + (RX_MAX - RX_MIN) * randNum;
        double rY = RY_MIN + (RY_MAX - RY_MIN) * randNum;
        Ellipse ellipse = new Ellipse(3 * Main.WINDOW_WIDTH/4.0, Main.WINDOW_HEIGHT/2, rX, rY);
        ellipse.setStroke(Color.BLACK);
        ellipse.setFill(Color.TRANSPARENT);
        ellipse.setStrokeWidth(2);
        ellipse.setRotate(45 * currentStars);

        Star star = new Star(ellipse);
        root.getChildren().add(1, star);
        //root.getChildren().add(ellipse);
        currentStars++;
    }
}
