package objects;

import javafx.animation.FadeTransition;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;

public class Cloud extends Group {

    private static final double TILE_SIZE = 18;

    private static final double[] POINTS = {
            2, 2,
            4, 1.5,
            6.5, 1.5,
            8, 2,
            8, 3.5,
            6, 4,
            3.5, 4,
            1.5, 3.5
    };

    private static final double[] CONTROL_POINTS = {
            0, 0,
            4, -1,
            7, -1,
            10, 0.5,
            10, 5,
            6, 6,
            3.5, 6,
            0, 5
    };

    public Cloud(double x, double y)
    {
        getChildren().addAll(getCloud());
        setOpacity(0);
        setTranslateX(x - getBoundsInParent().getWidth() / 2);
        setTranslateY(y - getBoundsInParent().getHeight() / 2);
    }

    private Path getCloud()
    {
        Path path = new Path();
        path.setStroke(Color.LIGHTSKYBLUE);
        path.setStrokeWidth(2);
        path.setFill(Color.WHITE);

        MoveTo offset = new MoveTo();
        offset.setX(POINTS[0] * TILE_SIZE);
        offset.setY(POINTS[1] * TILE_SIZE);
        path.getElements().add(offset);

        for(int i = 2; i < POINTS.length; i+= 2)
        {
            CubicCurveTo curve = new CubicCurveTo();
            curve.setX(TILE_SIZE * POINTS[i]);
            curve.setY(TILE_SIZE * POINTS[i + 1]);
            curve.setControlX1(TILE_SIZE * CONTROL_POINTS[i - 2]);
            curve.setControlY1(TILE_SIZE * CONTROL_POINTS[i - 1]);
            curve.setControlX2(TILE_SIZE * CONTROL_POINTS[i]);
            curve.setControlY2(TILE_SIZE * CONTROL_POINTS[i + 1]);
            path.getElements().add(curve);
        }

        CubicCurveTo curve = new CubicCurveTo();
        curve.setX(TILE_SIZE * POINTS[0]);
        curve.setY(TILE_SIZE * POINTS[1]);
        curve.setControlX1(TILE_SIZE * CONTROL_POINTS[CONTROL_POINTS.length - 2]);
        curve.setControlY1(TILE_SIZE * CONTROL_POINTS[CONTROL_POINTS.length - 1]);
        curve.setControlX2(TILE_SIZE * CONTROL_POINTS[0]);
        curve.setControlY2(TILE_SIZE * CONTROL_POINTS[1]);
        path.getElements().add(curve);

        ClosePath closePath = new ClosePath();
        path.getElements().add(closePath);

        return path;
    }

    private static final double FADE_DURATION = 1.5;
    public void activate() {
        FadeTransition ft = new FadeTransition(Duration.seconds(FADE_DURATION), this);
        ft.setToValue(1);
        ft.setFromValue(0);
        ft.play();
    }

    public void deactivate(Group root) {
        FadeTransition ft = new FadeTransition(Duration.seconds(FADE_DURATION), this);
        ft.setToValue(0);
        ft.setFromValue(1);
        ft.play();
    }
}
