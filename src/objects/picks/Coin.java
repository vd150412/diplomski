/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects.picks;

import java.util.Random;

import controllers.Config;
import objects.picks.PickController;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import objects.picks.PickItem;

/**
 *
 * @author dvucinic
 */
public class Coin extends PickItem {

    public static final double COIN_RADIUS = 20;

    public Coin(Random rand, PickController pickController)
    {
        super(rand, pickController);

        Circle body = new Circle(COIN_RADIUS);
        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE), new Stop(1, Color.YELLOW)};
        RadialGradient rg = new RadialGradient(225, 0.5, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, stops);
        body.setFill(rg);
        body.setStroke(Color.LIGHTGRAY);
        body.setStrokeWidth(3);
        
        double rectW = COIN_RADIUS * 0.6;
        Rectangle rect = new Rectangle(-rectW/2, -rectW/2, rectW, rectW);
        rect.setFill(Color.LIGHTGRAY);
        rect.setRotate(45);
        
        Rectangle rect2 = new Rectangle(-rectW/2, -rectW/2, rectW, rectW);
        rect2.setFill(Color.LIGHTGRAY);
        
        getChildren().addAll(body, rect, rect2);
    }

    @Override
    public void activate()
    {
        super.activate();

        ticksActive = (int) (Config.COIN_PICK_ACTIVE * 60);
    }
}
