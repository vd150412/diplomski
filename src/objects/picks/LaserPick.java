/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects.picks;

import java.util.Random;

import controllers.Config;
import objects.picks.PickController;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import objects.picks.PickItem;

/**
 *
 * @author dvucinic
 */
public class LaserPick extends PickItem {

    public static final double LASER_RADIUS = 20;

    public LaserPick(Random rand, PickController pickController)
    {
        super(rand, pickController);

        Circle body = new Circle(LASER_RADIUS);
        Stop[] stops = new Stop[] {new Stop(0, Color.YELLOW), new Stop(1, Color.ORANGE)};
        RadialGradient rg = new RadialGradient(225, 0.5, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, stops);
        body.setFill(rg);
        body.setStroke(Color.RED);
        body.setStrokeWidth(5);

        getChildren().addAll(body);
    }

    @Override
    public void activate()
    {
        super.activate();
        ticksActive = (int) (Config.LASER_PICK_ACTIVE * 60);
    }
}
