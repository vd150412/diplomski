package objects.picks;

import controllers.Config;
import objects.balls.Shot;
import states.Game;
import javafx.scene.Group;
import states.GameState;
import main.Main;

import java.util.List;

public class LaserPickController extends PickController {

    public static final int LASER_POINTS = 10;
    public static final int LASER_TRIGGERED_LIMIT = 4;

    private int laserTriggered = 0;
    private int ticksActive = 0;

    public LaserPickController(Group root)
    {
        super(root);
        for(int i = 0; i < NUM_OF_PICKS; i++)
        {
            LaserPick laserPick = new LaserPick(rand, this);
            pickList.add(laserPick);
        }
    }

    @Override
    public void update(List<Shot> shots) {
        super.update(shots);

        if(Main.instance.gameState == GameState.LASER_BONUS && (++ticksActive >= 60 * Config.LASER_DURATION || laserTriggered >= LASER_TRIGGERED_LIMIT))
        {
            Main.instance.gameState = GameState.GAME;
            Game.instance.laserController.disableLine();
        }
    }

    @Override
    public void onItemDeactivate() {
        Game.instance.textScore.updateScore(LASER_POINTS);
        Main.instance.gameState = GameState.LASER_BONUS;
        Game.instance.sun.activateLine();
        ticksActive = 0;
        laserTriggered = 0;
    }

    public void triggerLaser() {
        laserTriggered++;
    }

    @Override
    public int ticksToActivate() {
        int ticksMax = Config.LASER_FREQ <= 0.5 ? TICKS_MAX : TICKS_MAX - (int) ((TICKS_MAX - TICKS_MIN) * (Config.LASER_FREQ - 0.5) * 2);
        int ticksMin = Config.LASER_FREQ >= 0.5 ? TICKS_MIN : TICKS_MIN + (int) ((TICKS_MAX - TICKS_MIN) * (0.5 - Config.LASER_FREQ) * 2);
        return ticksMax == ticksMax ? ticksMin : rand.nextInt(ticksMax - ticksMin) + ticksMin;
    }
}