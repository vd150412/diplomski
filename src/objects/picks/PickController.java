/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects.picks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.scene.Group;
import objects.balls.Shot;

/**
 *
 * @author dvucinic
 */
public abstract class PickController {

    public static final int NUM_OF_PICKS = 10;
    public static final int TICKS_MIN = 60 * 1;
    public static final int TICKS_MAX = 60 * 20;

    protected Random rand;
    protected int ticksToActivate;
    protected ArrayList<PickItem> pickList = new ArrayList<>();
    protected int pickItemIndex = 0;
    protected Group root;

    public PickController(Group root)
    {
        this.rand = new Random();
        this.root = root;
        ticksToActivate = ticksToActivate();
    }

    public void update(List<Shot> shots)
    {
        for(PickItem pickItem : pickList)
        {
            if(pickItem.isActive())
            {
                pickItem.update();

                for(Shot shot : shots)
                {
                    if(pickItem.getBoundsInParent().intersects(shot.getBoundsInParent()))
                    {
                        pickItem.deactivate();
                        onItemDeactivate();
                    }
                }
            }
        }

        if(--ticksToActivate == 0)
        {
            PickItem pickToActivate = pickList.get(pickItemIndex);
            pickItemIndex = (pickItemIndex + 1) % pickList.size();
            pickToActivate.activate();
            root.getChildren().add(pickToActivate);

            ticksToActivate = ticksToActivate();
        }
    }

    public abstract void onItemDeactivate();

    public abstract int ticksToActivate();

    public void deactivate(PickItem pickItem)
    {
        root.getChildren().remove(pickItem);
    }
}
