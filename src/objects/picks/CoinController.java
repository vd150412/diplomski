/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects.picks;

import controllers.Config;
import objects.picks.Coin;
import objects.picks.PickController;
import states.Game;
import javafx.scene.Group;

/**
 *
 * @author dvucinic
 */
public class CoinController extends PickController {
    
    public static final int COIN_POINTS = 10;

    public CoinController(Group root)
    {
        super(root);
        for(int i = 0; i < NUM_OF_PICKS; i++)
        {
            Coin coin = new Coin(rand, this);
            pickList.add(coin);
        }
    }

    @Override
    public void onItemDeactivate() {
        Game.instance.textScore.updateScore(COIN_POINTS);
    }

    @Override
    public int ticksToActivate()
    {
        int ticksMax = Config.COIN_FREQ <= 0.5 ? TICKS_MAX : TICKS_MAX - (int) ((TICKS_MAX - TICKS_MIN) * (Config.COIN_FREQ - 0.5) * 2);
        int ticksMin = Config.COIN_FREQ >= 0.5 ? TICKS_MIN : TICKS_MIN + (int) ((TICKS_MAX - TICKS_MIN) * (0.5 - Config.COIN_FREQ) * 2);

        return ticksMax == ticksMax ? ticksMin : rand.nextInt(ticksMax - ticksMin) + ticksMin;
    }
}
