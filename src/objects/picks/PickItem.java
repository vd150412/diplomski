package objects.picks;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.util.Duration;
import main.Main;

import java.util.Random;

public abstract class PickItem extends Group {

    protected Random rand;
    protected PickController pickController;

    public static final double OFFSET_X;
    public static final double OFFSET_Y;

    public static final double MIN_X, MAX_X, MIN_Y, MAX_Y;
    public static final double[] EDGE_X, EDGE_Y;

    public static final int TICKS_ACTIVE_MIN = 60 * 2;
    public static final int TICKS_ACTIVE_MAX = 60 * 12;

    protected boolean active;
    protected int ticksActive;

    public PickItem(Random rand, PickController pickController)
    {
        this.rand = rand;
        this.pickController = pickController;
    }

    static
    {
        OFFSET_X = Main.WINDOW_WIDTH * 0.05;
        OFFSET_Y = Main.WINDOW_HEIGHT * 0.05;

        MIN_X = OFFSET_X;
        MAX_X = Main.WINDOW_WIDTH - OFFSET_X;
        MIN_Y = OFFSET_Y;
        MAX_Y = Main.WINDOW_HEIGHT - OFFSET_Y;

        EDGE_X = new double[]{MIN_X, MAX_X};
        EDGE_Y = new double[]{ MAX_Y};
    }

    public void update()
    {
        if(active && --ticksActive <= 0)
        {
            deactivate();
        }
    }

    public void activate()
    {
        setPickPosition();

        TranslateTransition tt = new TranslateTransition(Duration.seconds(1), this);
        tt.setToY(this.getTranslateY() + 5);
        tt.setCycleCount(Animation.INDEFINITE);
        tt.setAutoReverse(true);
        tt.play();

        active = true;
    }

    public void deactivate()
    {
        active = false;

        FadeTransition ft = new FadeTransition(Duration.seconds(0.25), this);
        ft.setToValue(0);
        ft.setOnFinished(e -> pickController.deactivate(this));
        ft.play();
    }
    public boolean isActive() {
        return active;
    }

    public void setPickPosition()
    {
        double x;
        double y;
        if(rand.nextBoolean())
        {
            x = rand.nextDouble() * (MAX_X - MIN_X) + MIN_X;
            y = EDGE_Y[0];
        }
        else
        {
            x = EDGE_X[rand.nextInt(2)];
            y = rand.nextDouble() * (MAX_Y - MIN_Y) + MIN_Y;
        }

        setTranslateX(x);
        setTranslateY(y);
    }
}
