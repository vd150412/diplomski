/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.util.Duration;

/**
 *
 * @author Korisnik
 */
public class Star extends Group {
    private int TILE_SIZE = 5;
    private static Random rand = new Random();
    
    private double points[] = {
        TILE_SIZE * 3, 0,
        TILE_SIZE * 3 + TILE_SIZE / 2, TILE_SIZE * 2,
        TILE_SIZE * 5 + TILE_SIZE / 2, TILE_SIZE * 2,
        TILE_SIZE * 4, TILE_SIZE * 3,
        TILE_SIZE * 4 + TILE_SIZE / 2, TILE_SIZE * 5,
        TILE_SIZE * 3, TILE_SIZE * 3 + TILE_SIZE / 2,
        TILE_SIZE + TILE_SIZE / 2, TILE_SIZE * 5,
        TILE_SIZE * 2, TILE_SIZE * 3,
        TILE_SIZE /2, TILE_SIZE * 2,
        TILE_SIZE * 2 + TILE_SIZE / 2, TILE_SIZE * 2
    };
    
    public Star(Ellipse ellipse)
    {
        Polygon polygon = new Polygon(points);
        polygon.setFill(Color.YELLOW);
        polygon.setStrokeWidth(2);
        polygon.setStroke(Color.GOLD);
        
        Duration t = Duration.seconds(7 + rand.nextInt(6));
        PathTransition pt = new PathTransition(t, ellipse, polygon);
        pt.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pt.setCycleCount(Animation.INDEFINITE);
        pt.setInterpolator(Interpolator.LINEAR);
        
        pt.play();
        
        RotateTransition rt = new RotateTransition(Duration.seconds(4), polygon);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.play();
        
        getChildren().addAll(polygon);
    }
}
