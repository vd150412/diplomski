package objects.balls;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import objects.balls.Ball;

public class BallReverse extends Ball {

    private static final double TILE_SIZE = BALL_RADIUS / 6;
    private static final double REVERSE_ICON_LEFT[] = {
            TILE_SIZE * -4, TILE_SIZE * 0,
            TILE_SIZE * -1, TILE_SIZE * 4,
            TILE_SIZE * 0, TILE_SIZE * 3,
            TILE_SIZE * -2.5, TILE_SIZE * 0,
            TILE_SIZE * 0, TILE_SIZE * -3,
            TILE_SIZE * -1, TILE_SIZE * -4
    };

    private static final double REVERSE_ICON_RIGHT[] = {
            TILE_SIZE * 0, TILE_SIZE * 0,
            TILE_SIZE * 2.5, TILE_SIZE * 3.5,
            TILE_SIZE * 3.5, TILE_SIZE * 2.5,
            TILE_SIZE * 1.5, TILE_SIZE * 0,
            TILE_SIZE * 3.5, TILE_SIZE * -2.5,
            TILE_SIZE * 2.5, TILE_SIZE * -3.5
    };

    public BallReverse()
    {
        super();

        Polygon polygonLeft = new Polygon(REVERSE_ICON_LEFT);
        polygonLeft.setFill(Color.WHITE);
        polygonLeft.setStroke(Color.BLACK);

        Polygon polygonRight = new Polygon(REVERSE_ICON_RIGHT);
        polygonRight.setFill(Color.WHITE);
        polygonRight.setStroke(Color.BLACK);

        getChildren().addAll(polygonLeft, polygonRight);
    }
}
