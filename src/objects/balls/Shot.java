/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects.balls;

import controllers.BallController;
import objects.Sun;
import objects.balls.Ball;
import states.Game;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;

/**
 *
 * @author km183142m
 */
public class Shot extends Ball {

    private static final double SHOT_VELOCITY = -13 * 1.5;

    private enum ShotState {
        SHOT, MOVING
    }

    private double velocityX, velocityY;
    private ShotState shotState = ShotState.SHOT;

    public Shot(Sun sun) {
        this.color = sun.getMouthColor();
        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE), new Stop(1, color)};
        radialGradient = new RadialGradient(225, 0.5, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, stops);
        body.setFill(Game.instance.shotMissController.ballsBlack() ? radialGradientBlack : radialGradient);
        body.setStroke(Color.BLACK);

        velocityX = SHOT_VELOCITY * Math.sin(Math.toRadians(sun.getRotate()));
        velocityY = -SHOT_VELOCITY * Math.cos(Math.toRadians(sun.getRotate()));
        setTranslateX(sun.getTranslateX() - Sun.SUN_RADIUS / 4.0 * Math.sin(Math.toRadians(sun.getRotate())));
        setTranslateY(sun.getTranslateY() + Sun.SUN_RADIUS / 4.0 * Math.cos(Math.toRadians(sun.getRotate())));
    }

    public void becomeMoving(Ball neighbor) {
        shotState = ShotState.MOVING;
        currentPath = neighbor.currentPath;
        setTranslateX(neighbor.getTranslateX());
        setTranslateY(neighbor.getTranslateY());
        velocityPath = BallController.instance.getBallVelocity();
    }

    @Override
    public void update()
    {
        if (shotState.equals(ShotState.SHOT))
        {
            double x = getTranslateX();
            double y = getTranslateY();
            setTranslateX(x + velocityX);
            setTranslateY(y + velocityY);
        }
        else
        {
            super.update();
        }
    }
}
