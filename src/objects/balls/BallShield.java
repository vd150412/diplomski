/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects.balls;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import objects.balls.Ball;

/**
 *
 * @author dvucinic
 */
public class BallShield extends Ball {

    private static final double SHIELD_STROKE = Ball.BALL_RADIUS / 3.5;
    private Circle shield, shield2;
    private boolean hitted;
    
    public BallShield()
    {
        super();
        shield = new Circle(BALL_RADIUS  - SHIELD_STROKE / 2);
        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE),  new Stop(0.2, Color.WHITE), new Stop(0.8, Color.DARKGRAY), new Stop(1, Color.BLACK)};
        LinearGradient lg = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops);
        RadialGradient rg = new RadialGradient(225, 0.5, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, stops);
        shield.setFill(Color.TRANSPARENT);
        shield.setStroke(lg);
        shield.setStrokeWidth(SHIELD_STROKE);

        shield2 = new Circle(BALL_RADIUS - SHIELD_STROKE);
        shield2.setFill(Color.TRANSPARENT);
        shield2.setStroke(Color.BLACK);

        getChildren().addAll(shield, shield2);
    }
    
    public void removeShield()
    {
        getChildren().removeAll(shield, shield2);
        hitted = true;
    }
    
    @Override
    public boolean canRemove() { 
        return hitted;
    }
    
    @Override
    public int calcScore() {
        if(color.equals(Color.YELLOW)) return 3;
        return 2;
    }
}
