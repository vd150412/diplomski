package objects.balls;

import objects.balls.Ball;
import states.Game;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.ScaleTransition;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.util.Duration;

public class BallBomb extends Ball {

    private Polygon bombPolygon;

    public BallBomb()
    {
        super();

        getChildren().removeAll(body);
        getChildren().addAll(generateBombStar());
    }

    private Polygon generateBombStar()
    {
        double[] points = new double[32];
        for(int i = 0; i < 8; i++)
        {
            double sina = Math.sin(Math.toRadians(45 * i));
            double cosa = Math.cos(Math.toRadians(45 * i));

            double sina2 = Math.sin(Math.toRadians(45 * i + 22.5));
            double cosa2 = Math.cos(Math.toRadians(45 * i + 22.5));

            points[i * 4] = sina * BALL_RADIUS;
            points[i * 4 + 1] = cosa * BALL_RADIUS;

            points[i * 4 + 2] = sina2 * BALL_RADIUS / 1.5;
            points[i * 4 + 3] = cosa2 * BALL_RADIUS / 1.5;
        }

        bombPolygon = new Polygon(points);
        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE), new Stop(1, color)};
        radialGradient = new RadialGradient(0, 0, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, stops);
        bombPolygon.setFill(Game.instance.shotMissController.ballsBlack() ? radialGradientBlack : radialGradient);
        bombPolygon.setStroke(Color.BLACK);

        ScaleTransition st = new ScaleTransition(Duration.seconds(0.6), bombPolygon);
        final double bombScale = 0.9;
        st.setToX(bombScale);
        st.setToY(bombScale);
        st.setInterpolator(Interpolator.LINEAR);
        st.setCycleCount(Animation.INDEFINITE);
        st.setAutoReverse(true);
        st.play();

        return bombPolygon;
    }

    public void selectBall() {
        bombPolygon.setStrokeWidth(5);
    }

    public void deselectBall() {
        bombPolygon.setStrokeWidth(1);
    }

    public void toBlack() {
        bombPolygon.setFill(radialGradientBlack);
    }

    public void fromBlack() {
        bombPolygon.setFill(radialGradient);
    }
}
