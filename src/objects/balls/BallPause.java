package objects.balls;

import javafx.scene.paint.*;
import javafx.scene.shape.Rectangle;
import objects.balls.Ball;

public class BallPause extends Ball {

    private static final double RECT_HEIGHT = BALL_RADIUS ;
    private static final double RECT_WIDTH = BALL_RADIUS / 4;

    private static final double RECT_X = BALL_RADIUS / 8;
    private static final double RECT_Y = BALL_RADIUS / 2;

    public BallPause()
    {
        super();

        Rectangle rect1 = new Rectangle(-RECT_X - RECT_WIDTH, -RECT_Y, RECT_WIDTH, RECT_HEIGHT);
        Rectangle rect2 = new Rectangle(RECT_X, -RECT_Y, RECT_WIDTH, RECT_HEIGHT);

        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE), new Stop(1, Color.BLACK)};
        LinearGradient lg = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops);

        rect1.setFill(Color.WHITE);
        rect1.setStroke(Color.BLACK);

        rect2.setFill(Color.WHITE);
        rect2.setStroke(Color.BLACK);

        getChildren().addAll(rect1, rect2);
    }
}
