/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects.balls;

import controllers.BallController;
import states.Game;
import javafx.scene.Group;
import javafx.scene.paint.*;
import javafx.scene.shape.Circle;
import javafx.util.Pair;
import controllers.LevelController;
import main.Main;

/**
 *
 * @author km183142m
 */
public class Ball extends Group {

    public static final double BALL_RADIUS = 20;

    private static final Color[] colors = {Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW};
    protected Color color;

    protected Circle body;
    protected int currentPath = 0;
    protected int velocityPath = 1;

    protected static final RadialGradient radialGradientBlack =
            new RadialGradient(225, 0.5, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE,
            new Stop[] {new Stop(0, Color.WHITE), new Stop(1, Color.BLACK)});

    protected RadialGradient radialGradient;

    public Ball()
    {
        this(1 / 8.0 * Main.WINDOW_WIDTH, 0);
        setTranslateX(LevelController.instance.getX(0));
        setTranslateY(LevelController.instance.getY(0));
    }

    public Ball(double x, double y) {
        body = new Circle(BALL_RADIUS);
        color = colors[(int) (Math.random() * colors.length)];
        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE), new Stop(1, color)};
        radialGradient = new RadialGradient(225, 0.5, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, stops);
        body.setFill(Game.instance.shotMissController.ballsBlack() ? radialGradientBlack : radialGradient);
        body.setStroke(Color.BLACK);
        getChildren().addAll(body);

        this.velocityPath = BallController.instance.getBallVelocity();
        setTranslateX(x);
        setTranslateY(y);
    }

    public Color getColor() {
        return color;
    }

    public void update()
    {
        Pair<Float, Float> path = LevelController.instance.getPath(currentPath);
        currentPath += velocityPath;
        setTranslateX(path.getKey());
        setTranslateY(path.getValue());
    }

    public void reversePath(int times)
    {
        currentPath = LevelController.instance.reverseIndex(currentPath, times);
        Pair<Float, Float> path = LevelController.instance.getPath(currentPath);
        setTranslateX(path.getKey());
        setTranslateY(path.getValue());
    }

    public boolean canRemove() {
        return true;
    }

    public int calcScore() {
        if(color.equals(Color.YELLOW)) return 2;
        return 1;
    }

    public boolean canAddBall() {
        return currentPath >= LevelController.FIRST_INDEX;
    }

    public void setVelocity(int velocityPath) {
        this.velocityPath = velocityPath;
    }

    public int getCurrentPath() {
        return currentPath;
    }

    protected boolean selected = false;
    public void updateSelected(boolean toSelect)
    {
        if(toSelect)
        {
            if(selected == false)
            {
                selectBall();
                selected = true;
            }
        }
        else
        {
            if(selected)
            {
                deselectBall();
                selected = false;
            }
        }
    }

    public void selectBall() {
        body.setStroke(Color.WHITE);
        body.setStrokeWidth(5);
    }

    public void deselectBall() {
        body.setStroke(Color.BLACK);
        body.setStrokeWidth(1);
    }

    public void toBlack() {
        body.setFill(radialGradientBlack);
    }

    public void fromBlack() {
        body.setFill(radialGradient);
    }

}
