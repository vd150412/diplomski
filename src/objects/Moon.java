/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.*;
import javafx.util.Duration;
import main.Main;

/**
 *
 * @author km183142m
 */
public class Moon extends Group {

    private static final double MOON_RADIUS = 50;

    private Circle body;
    private Path[] eyes = new Path[2];
    private Circle mouth;

    private Circle pupils[] = new Circle[2];
    private Path[] eyesBack = new Path[2];
    private ScaleTransition scaleTransitionMouth;
    private TranslateTransition translateTransitionMouth;
    
    private ZController moonZController;
    private Rectangle collision;
    
    public Moon(double x, double y, Group root) {

        body = new Circle(MOON_RADIUS);
        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE), new Stop(1, Color.LIGHTSKYBLUE)};
        RadialGradient rg = new RadialGradient(180, 1, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, stops);
        body.setFill(rg);
        moonZController = new ZController(root, x, y);
        
        for(int i = 0; i < 2; i++)
        {
            double startX = MOON_RADIUS/2;
            double endX = 5;
            double startY = -5;
            double eyeH = MOON_RADIUS/1.5;
            double eyeW = (startX - endX)/2;
            double eyeR = eyeW * 0.7;
            
            eyes[i] = getEye(i == 0 ? -1 : 1, startX, endX, startY, eyeH);
            eyes[i].setStroke(Color.BLACK);
            eyes[i].setStrokeWidth(2);
            eyes[i].setFill(Color.LIGHTGRAY);
            
            eyesBack[i] = getEye(i == 0 ? -1 : 1, startX, endX, startY, eyeH);
            eyesBack[i].setStroke(Color.BLACK);
            eyesBack[i].setStrokeWidth(2);
            eyesBack[i].setFill(Color.WHITE);
            
            pupils[i] = getPupil(i == 0 ? -1 : 1, endX + eyeW, startY - eyeR, eyeR);
        }
        
        setMouth();

        Group sleepHat = getSleepHat();
        getChildren().addAll(body, eyesBack[0], eyesBack[1], pupils[0], pupils[1], eyes[0], eyes[1], mouth);
        getChildren().addAll(sleepHat.getChildren());
        setTranslateX(x);
        setTranslateY(y);

        double sin45 = Math.sin(Math.toRadians(45));
        double collX = x - sin45 * MOON_RADIUS;
        double collY = y - sin45 * MOON_RADIUS;
        double collW = sin45 * MOON_RADIUS * 2;
        double collH = sin45 * MOON_RADIUS * 2;
       collision = new Rectangle(collX, collY, collW, collH);
       collision.setFill(Color.RED);
       root.getChildren().add(collision);
    }

    public void onGameOver()
    {
        removeZ();
        for(int i = 0; i < 2; i++)
        {
            MoveTo moveTo = (MoveTo) eyes[i].getElements().get(0);
            CubicCurveTo cubicCurveTo = (CubicCurveTo) eyes[i].getElements().get(1);
            
            double offsetCtrl = (i == 0 ? -1 : 1) * 5;
            double offset = 12;
            double timeMove = 0.25;
            KeyValue moveTo1 = new KeyValue(moveTo.yProperty(), moveTo.getY() - offset, Interpolator.EASE_BOTH);
            KeyValue moveTo2 = new KeyValue(moveTo.yProperty(), moveTo.getY(), Interpolator.EASE_BOTH);
            KeyValue cubicTo1 = new KeyValue(cubicCurveTo.yProperty(), cubicCurveTo.getY() - offset, Interpolator.EASE_BOTH);
            KeyValue cubicTo2 = new KeyValue(cubicCurveTo.yProperty(), cubicCurveTo.getY(), Interpolator.EASE_BOTH);
            KeyValue ctrlX1_1 = new KeyValue(cubicCurveTo.controlX1Property(), cubicCurveTo.getControlX1() - offsetCtrl, Interpolator.EASE_BOTH);
            KeyValue ctrlX1_2 = new KeyValue(cubicCurveTo.controlX1Property(), cubicCurveTo.getControlX1(), Interpolator.EASE_BOTH);
            KeyValue ctrlX2_1 = new KeyValue(cubicCurveTo.controlX2Property(), cubicCurveTo.getControlX2() + offsetCtrl, Interpolator.EASE_BOTH);
            KeyValue ctrlX2_2 = new KeyValue(cubicCurveTo.controlX2Property(), cubicCurveTo.getControlX2(), Interpolator.EASE_BOTH);
           
            Timeline t = new Timeline(
                    new KeyFrame(Duration.seconds(timeMove * 1), moveTo1, cubicTo1, ctrlX1_1, ctrlX2_1),
                     new KeyFrame(Duration.seconds(timeMove * 2), moveTo2, cubicTo2, ctrlX1_2, ctrlX2_2),
                     new KeyFrame(Duration.seconds(timeMove * 3), moveTo1, cubicTo1, ctrlX1_1, ctrlX2_1),
                     new KeyFrame(Duration.seconds(timeMove * 4), moveTo2, cubicTo2, ctrlX1_2, ctrlX2_2),
                     new KeyFrame(Duration.seconds(timeMove * 5), e -> animationEnd(), moveTo1, cubicTo1, ctrlX1_1, ctrlX2_1)
            );
            
            t.play();
            scaleTransitionMouth.stop();
            translateTransitionMouth.stop();
        }
    }

    private boolean eyesRemoved = false;
    private void animationEnd()
    {
        if(eyesRemoved == false)
        {
            getChildren().removeAll(eyes[0], eyes[1]);
            Main.instance.onGameOver();
            eyesRemoved = true;
        }
    }
        
    private void setMouth()
    {
        mouth = new Circle(0, 20, MOON_RADIUS / 10);
        mouth.setFill(Color.BLACK);
        
        Duration d = Duration.seconds(1);
        
        scaleTransitionMouth = new ScaleTransition(d, mouth);
        double scale = 1.6;
        scaleTransitionMouth.setToY(scale);
        scaleTransitionMouth.setToX(scale);
        scaleTransitionMouth.setAutoReverse(true);
        scaleTransitionMouth.setCycleCount(Animation.INDEFINITE);
        scaleTransitionMouth.play();
        
        double offsetX = -4, offsetY = 3;
        translateTransitionMouth = new TranslateTransition(d, mouth);
        translateTransitionMouth.setToX(offsetX);
        translateTransitionMouth.setToY(offsetY);
        translateTransitionMouth.setAutoReverse(true);
        translateTransitionMouth.setCycleCount(Animation.INDEFINITE);
        translateTransitionMouth.play();
    }
    
    private Circle getPupil(int i, double centerX, double centerY, double r)
    {
        Circle circle = new Circle(i * centerX, centerY, r);
        circle.setFill(Color.BLACK);
        
        return circle;
    }
    
    private Path getEye(double i, double startX, double endX, double eyeY, double eyeH)
    {
        Path path = new Path();
        path.setStroke(Color.BLACK);
        path.setFill(Color.WHITE);
        
        startX *= i;
        endX *= i;
        
        MoveTo offset = new MoveTo();
        offset.setX(startX);
        offset.setY(eyeY);
        
        CubicCurveTo curve = new CubicCurveTo();
        curve.setX(endX);
        curve.setY(eyeY);
        curve.setControlX1(offset.getX() + 5*i);
        curve.setControlY1(eyeY - eyeH);
        curve.setControlX2(curve.getX() - 5*i);
        curve.setControlY2(eyeY - eyeH);
        
        ClosePath closePath = new ClosePath();
        path.getElements().addAll(offset, curve, closePath);
        
        return path;
    }
    
    private Group getSleepHat() {
        Group hatRoot = new Group();

        Rectangle rect = new Rectangle(-MOON_RADIUS, -MOON_RADIUS*1.2, MOON_RADIUS*2, MOON_RADIUS/2.5);
        rect.setStroke(Color.BLACK);
        rect.setFill(Color.WHITE);
        rect.setArcHeight(20);
        rect.setArcWidth(20);
        rect.setStrokeWidth(2);
        
        double endX = MOON_RADIUS*1.75;
        double endY = -MOON_RADIUS*1.5;
        double startX = -MOON_RADIUS * 0.75;
        double startY = -MOON_RADIUS;
        
        Path path = new Path();
        path.setStroke(Color.BLACK);
        Stop[] stops = new Stop[] {new Stop(0, Color.WHITE), new Stop(0.5, Color.LIGHTSEAGREEN), new Stop(1, Color.LIGHTSEAGREEN)};
        LinearGradient lg = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops);
        path.setFill(lg);
        
        MoveTo moveTo = new MoveTo();
        moveTo.setX(startX);
        moveTo.setY(startY);
        
        QuadCurveTo curveTo1 = new QuadCurveTo();
        curveTo1.setX(endX);
        curveTo1.setY(endY);
        curveTo1.setControlX(-MOON_RADIUS/2);
        curveTo1.setControlY(-MOON_RADIUS*3.5); 
        
        QuadCurveTo curveTo2 = new QuadCurveTo();
        curveTo2.setX(MOON_RADIUS*0.75);
        curveTo2.setY(startY);
        curveTo2.setControlX(-MOON_RADIUS/4);
        curveTo2.setControlY(-MOON_RADIUS*2.5); 
        
        ClosePath closePath = new ClosePath();
        
        path.getElements().addAll(moveTo, curveTo1, curveTo2, closePath);
                
        Circle circle = new Circle(endX-12, endY-12, 18);
        circle.setStroke(Color.BLACK);
        circle.setFill(Color.WHITE);
        circle.setStrokeWidth(2);
        
        hatRoot.getChildren().addAll(path, rect, circle);
        
        return hatRoot;
    }

    public void update() {
        moonZController.update();
    }
    
    public void removeZ() {
        moonZController.makeInactive();
    }

    public Bounds getMoonBounds() {
        return collision.getBoundsInParent();
    }
}
