/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import javafx.scene.Group;
import javafx.scene.paint.*;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author km183142m
 */
public class Background extends Group{
    
    public Background(double width, double height){
        Rectangle background = new Rectangle(0, 0, width + 10, height + 10);
        Stop[] stops = new Stop []{
                new Stop(0, Color.LIGHTGRAY),
                new Stop(0.2, Color.DARKGRAY),
                new Stop(1, Color.BLACK)
        };

        LinearGradient lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
        background.setFill(lg);
        getChildren().add(background);
    }
    
}
