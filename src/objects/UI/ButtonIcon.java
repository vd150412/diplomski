package objects.UI;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import main.Main;
import states.GameMenu;

public class ButtonIcon extends Group {

    protected static final Font MENU_ITEM_FONT = Font.font("Courier New", 20);
    private EventHandler handler;

    public static final double RECT_WIDTH = 90;
    public static final double RECT_HEIGHT = 40;

    private static final Color STROKE_NORMAL = Color.LIGHTGRAY;
    private static final Color STROKE_SELECTED = GameMenu.MENU_ITEM_STROKE_COLOR;

    private Rectangle rect;

    public ButtonIcon(String textStr, double x, double y, Paint bgColor, EventHandler<MouseEvent> handler) {
        this.handler = handler;

        rect = new Rectangle(x, y, RECT_WIDTH, RECT_HEIGHT);
        final double gray = 0.35;
        rect.setFill(Color.color(gray, gray, gray, 0.5));
        rect.setStroke(Color.LIGHTGRAY);
        rect.setStrokeWidth(2);
        rect.setArcHeight(10);
        rect.setArcWidth(10);

        double textX = x + RECT_WIDTH / 2;
        Text text = new Text(textX, y, textStr);
        text.setFont(MENU_ITEM_FONT);
        text.setFill(bgColor);
        text.setX(textX - text.getBoundsInParent().getWidth() / 2);
        text.setY(y + RECT_HEIGHT / 2 + text.getBoundsInParent().getHeight() / 4);
        text.setTextAlignment(TextAlignment.JUSTIFY);
        text.setStyle("-fx-font-weight: bold");

        getChildren().addAll(rect, text);
    }

    public void handle(MouseEvent event) {
        handler.handle(event);
    }

    public boolean intersects(Node node) {
        return node.getParent() == this;
    }

    public void select() {
        rect.setStroke(STROKE_SELECTED);
        rect.setStrokeWidth(4);
    }

    public void deselect() {
        rect.setStroke(STROKE_NORMAL);
        rect.setStrokeWidth(2);
    }
}