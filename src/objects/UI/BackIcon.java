package objects.UI;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;

public class BackIcon extends GameIcon {

    private static final double TILE_SIZE = ICON_RADIUS / 6;
    private static final double REVERSE_ICON_LEFT[] = {
            TILE_SIZE * -4, TILE_SIZE * 0,
            TILE_SIZE * -1, TILE_SIZE * 4,
            TILE_SIZE * 0, TILE_SIZE * 3,
            TILE_SIZE * -2.5, TILE_SIZE * 0,
            TILE_SIZE * 0, TILE_SIZE * -3,
            TILE_SIZE * -1, TILE_SIZE * -4
    };

    private static final double REVERSE_ICON_RIGHT[] = {
            TILE_SIZE * 0, TILE_SIZE * 0,
            TILE_SIZE * 2.5, TILE_SIZE * 3.5,
            TILE_SIZE * 3.5, TILE_SIZE * 2.5,
            TILE_SIZE * 1.5, TILE_SIZE * 0,
            TILE_SIZE * 3.5, TILE_SIZE * -2.5,
            TILE_SIZE * 2.5, TILE_SIZE * -3.5
    };

    private EventHandler<MouseEvent> handler;
    protected Polygon polygonLeft, polygonRight;
    protected Group polygons;

    public BackIcon(double x, double y, Paint bgColor, EventHandler<MouseEvent> handler) {
        super(x, y, bgColor);

        this.handler = handler;

        polygonLeft = new Polygon(REVERSE_ICON_LEFT);
        polygonLeft.setFill(Color.WHITE);
        polygonLeft.setStroke(Color.BLACK);

        polygonRight = new Polygon(REVERSE_ICON_RIGHT);
        polygonRight.setFill(Color.WHITE);
        polygonRight.setStroke(Color.BLACK);

        polygons = new Group();
        polygons.getChildren().addAll(polygonLeft, polygonRight);

        getChildren().addAll(polygons);
    }

    @Override
    public void handle(MouseEvent event) {
        handler.handle(event);
    }

    @Override
    public boolean intersects(Node node) {

        if(node.getParent() == this) return true;

        if(node.getParent() != null)
        {
            if(node.getParent().getParent() == this) return true;
        }

        return false;
    }
}
