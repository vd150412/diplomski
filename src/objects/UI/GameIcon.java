package objects.UI;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import main.Main;
import states.GameState;

public abstract class GameIcon extends Group implements EventHandler<MouseEvent> {

    public static final double ICON_RADIUS = 30;

    public GameIcon(double x, double y, Paint bgColor)
    {
        this.setOnMouseClicked(this);

        Circle body = new Circle(ICON_RADIUS);
        body.setFill(bgColor);
        body.setStroke(Color.LIGHTGRAY);
        body.setStrokeWidth(2);

        getChildren().addAll(body);

        setTranslateX(x);
        setTranslateY(y);
    }

    public boolean intersects(Node node) {
        return node.getParent() == this;
    }
}