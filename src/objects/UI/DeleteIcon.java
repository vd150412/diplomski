package objects.UI;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;

public class DeleteIcon extends GameIcon {

    private EventHandler<MouseEvent> handler;

    private static final double TILE_SIZE = ICON_RADIUS / 6;
    private static final double POLYGON_POINTS[] = {
            TILE_SIZE * 0, TILE_SIZE * 1,
            TILE_SIZE * 2, TILE_SIZE * 3,
            TILE_SIZE * 3, TILE_SIZE * 2,
            TILE_SIZE * 1, TILE_SIZE * 0,

            TILE_SIZE * 3, TILE_SIZE * -2,
            TILE_SIZE * 2, TILE_SIZE * -3,
            TILE_SIZE * 0, TILE_SIZE * -1,
            TILE_SIZE * -2, TILE_SIZE * -3,

            TILE_SIZE * -3, TILE_SIZE * -2,
            TILE_SIZE * -1, TILE_SIZE * 0,
            TILE_SIZE * -3, TILE_SIZE * 2,
            TILE_SIZE * -2, TILE_SIZE * 3,
    };

    public DeleteIcon(double x, double y, Paint bgColor, EventHandler<MouseEvent> handler) {
        super(x, y, bgColor);

        this.handler = handler;
        Polygon polygon = new Polygon(POLYGON_POINTS);
        polygon.setFill(Color.WHITE);
        polygon.setStroke(Color.BLACK);

        getChildren().addAll(polygon);
    }

    @Override
    public void handle(MouseEvent event) {
        handler.handle(event);
    }
}
