package objects.UI;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import main.Main;
import states.GameState;

public class PauseIcon extends GameIcon {

    private static final double RECT_HEIGHT = ICON_RADIUS ;
    private static final double RECT_WIDTH = ICON_RADIUS / 4;

    private static final double RECT_X = ICON_RADIUS / 8;
    private static final double RECT_Y = ICON_RADIUS / 2;

    public PauseIcon(double x, double y, Paint bgColor)
    {
        super(x, y, bgColor);

        Rectangle rect1 = new Rectangle(-RECT_X - RECT_WIDTH, -RECT_Y, RECT_WIDTH, RECT_HEIGHT);
        Rectangle rect2 = new Rectangle(RECT_X, -RECT_Y, RECT_WIDTH, RECT_HEIGHT);

        rect1.setFill(Color.WHITE);
        rect1.setStroke(Color.BLACK);

        rect2.setFill(Color.WHITE);
        rect2.setStroke(Color.BLACK);

        getChildren().addAll(rect1, rect2);
    }

    @Override
    public void handle(MouseEvent event)
    {
        if(Main.instance.gameState != GameState.GAME_OVER)
        {
            Main.instance.changeState(Main.instance.pauseMenu);
        }
    }
}
