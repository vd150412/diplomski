package objects.UI;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;

public class ForwardIcon extends BackIcon {

    public ForwardIcon(double x, double y, Paint bgColor, EventHandler<MouseEvent> handler) {
        super(x, y, bgColor, handler);

        polygons.setScaleX(-1);
    }
}
