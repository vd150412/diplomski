/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.util.Duration;

/**
 *
 * @author Korisnik
 */
public class Z extends Group {

    private static final double TILE_SIZE = 5;
    private float velocity = 2.1f;
    private Polygon polygon;
    private double[] points = {
        0, 0, 
        4 * TILE_SIZE, 0,
        4 * TILE_SIZE, TILE_SIZE,
        TILE_SIZE, 4 * TILE_SIZE,
        4 * TILE_SIZE, 4 * TILE_SIZE,
        4 * TILE_SIZE, 5 * TILE_SIZE,
        0, 5 * TILE_SIZE,
        0, 4 * TILE_SIZE,
        3 * TILE_SIZE, TILE_SIZE,
        0, TILE_SIZE
    };
    
    private boolean inScene = false;
    private boolean active = false;
    private double startX, startY;
    
    public Z(double x, double y)
    {
        polygon = new Polygon(points);
        polygon.setFill(Color.WHITE);
        polygon.setStroke(Color.BLACK);
        polygon.setTranslateY(-4 * TILE_SIZE);
        polygon.setTranslateX(-2 * TILE_SIZE);
        RotateTransition rt = new RotateTransition(Duration.seconds(1), polygon);
        rt.setAutoReverse(true);
        rt.setCycleCount(Animation.INDEFINITE);
        rt.setFromAngle(-45);
        rt.setToAngle(45);
        rt.play();
        
        getChildren().addAll(polygon);
        startX = x;
        startY = y;
        setTranslateX(x);
        setTranslateY(y);
    }
    
    public void update() 
    {
        if(!active) return;
        setTranslateY(getTranslateY()- velocity);
        if(getTranslateY() < -100)
        {
            active = false;
        }
    }
    
    public void makeInactive()
    {
        active = false;
        setTranslateX(startX);
        setTranslateY(startY);
    }
    
    public void makeActive()
    {
        active = true;
        setTranslateX(startX);
        setTranslateY(startY);
    }
}
