/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import states.Game;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.util.Duration;
import states.GameState;
import main.Main;

/**
 *
 * @author km183142m
 */
public class Sun extends Group {

    public static final double SUN_RADIUS = 70;

    public static final Color[] colors = {Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW};

    private Group root;
    private Circle body;
    private Circle mouth, mouthNext;
    private Color mouthColor, mouthNextColor;
    
    private Rectangle[] rays;
    private Rectangle[] cheeks;
    private Path[] eyes, eyesFront;
    private Circle[] pupils;
    
    private ZController sunZController;
    private int ticksInactive = 0;
    private boolean inactive = false;
    private int TICKS_LIMIT = 60*10;
    
    private Timeline[] sunAnimation = new Timeline[2];
    private double eyeFrontStartY, eyeFrontStartH;
    private double eyeFrontStartCtrlX1, eyeFrontStartCtrlX2;
    private double eyeFrontOffset;
    MoveTo eyeFrontMoveTo[] = new MoveTo[2];
    CubicCurveTo eyeFrontCurveTo[] = new CubicCurveTo[2];

    public double lastX, lastY;
    private Cloud[] clouds = new Cloud[2];

    public Sun(double x, double y, Group root)
    {
        this.root = root;
        sunZController = new ZController(root, x, y);
        
        body = new Circle(SUN_RADIUS);
        body.setFill(Color.GOLD);
        body.setStroke(Color.ORANGE);
        body.setStrokeWidth(2);
        
        mouth = new Circle(SUN_RADIUS / 3 - 5);
        mouthColor = colors[(int) (Math.random() * colors.length)];
        mouth.setFill(mouthColor);
        mouth.setTranslateY(SUN_RADIUS / 4);
        
        mouthNext = new Circle(SUN_RADIUS/3);
        mouthNextColor = colors[(int) (Math.random() * colors.length)];
        mouthNext.setFill(mouthNextColor);
        mouthNext.setTranslateY(SUN_RADIUS/4);
        
        rays = new Rectangle[2];
        for(int i = 0; i < rays.length; i++)
        {
            rays[i] = new Rectangle(-SUN_RADIUS, -SUN_RADIUS, SUN_RADIUS*2, SUN_RADIUS*2);
            rays[i].setFill(Color.YELLOW);
            ScaleTransition st = new ScaleTransition(Duration.seconds(0.9), rays[i]);
            float finalScale = 1.2f;
            st.setToX(finalScale);
            st.setToY(finalScale);
            st.setAutoReverse(true);
            st.setCycleCount(Animation.INDEFINITE);
            st.play();
        }
        rays[0].setRotate(45);
        
        cheeks = new Rectangle[2];
        double cheekW = SUN_RADIUS / 4.5f;
        double cheekH = SUN_RADIUS / 6.5f;
        for(int i = 0; i < cheeks.length; i++)
        {
            double cheekX = i == 0 ? -SUN_RADIUS/2 - cheekW/2 : SUN_RADIUS/2 - cheekW/2;
            cheeks[i] = new Rectangle(cheekX, -cheekH, cheekW, cheekH);
            cheeks[i].setArcHeight(7.5f);
            cheeks[i].setArcWidth(7.5f);
            cheeks[i].setFill(Color.CORAL);
        }
        
        eyes = new Path[2];
        eyesFront = new Path[2];
        pupils = new Circle[2];
        double startX = cheekW + 7.5;
        double endX = 5;
        double startY = -1.5 * cheekH;
        double eyeH = cheekH * 3.1;
        double eyeW = (startX - endX)/2;
        double eyeR = eyeW * 0.7;
        eyeFrontOffset = 22;
        eyeFrontStartY = startY - eyeFrontOffset;
        eyeFrontStartH = eyeH - eyeFrontOffset;
        eyeFrontStartCtrlX1 = startX - 5;
        eyeFrontStartCtrlX2 = endX + 5;
        for(int i = 0; i < 2; i++)
        {  
            eyes[i] = getEye(i == 0 ? -1 : 1, startX, endX, startY, eyeH);
            eyes[i].setFill(Color.WHITE);       

            eyesFront[i] = getFrontEye(i, startX, endX, eyeFrontStartY, eyeFrontStartH, eyeFrontStartCtrlX1, eyeFrontStartCtrlX2);
            eyesFront[i].setFill(Color.LIGHTGRAY);
            
            sunAnimation[i] = createAnimation(i);
            
            pupils[i] = getPupil(i == 0 ? -1 : 1, endX + eyeW, startY - eyeR, eyeR);
        }
              
        getChildren().addAll(rays[0], rays[1], body, mouthNext, mouth);
        for(int i = 0; i < 2; i++)
        {
            getChildren().addAll(cheeks[i], eyes[i], pupils[i]);
        }

        final double cloudOffset = 30;
        clouds[0] = new Cloud(x - cloudOffset, y - 10);
        clouds[1] = new Cloud(x + cloudOffset, y + 35);
        clouds[1].setRotate(180);

        setTranslateX(x);
        setTranslateY(y);
    }
    
    private Timeline createAnimation(int i)
    {
        MoveTo moveTo = (MoveTo) eyesFront[i].getElements().get(0);
        CubicCurveTo cubicCurveTo = (CubicCurveTo) eyesFront[i].getElements().get(1);

        double offsetCtrl = (i == 0 ? -1 : 1) * 10;
        double timeMove = 2;   
        
        KeyValue moveTo2 = new KeyValue(moveTo.yProperty(), moveTo.getY() + eyeFrontOffset, Interpolator.EASE_BOTH);
        KeyValue cubicTo2 = new KeyValue(cubicCurveTo.yProperty(), cubicCurveTo.getY() + eyeFrontOffset, Interpolator.EASE_BOTH);
        KeyValue ctrlX1_2 = new KeyValue(cubicCurveTo.controlX1Property(), cubicCurveTo.getControlX1() + offsetCtrl, Interpolator.EASE_BOTH);
        KeyValue ctrlX2_2 = new KeyValue(cubicCurveTo.controlX2Property(), cubicCurveTo.getControlX2() - offsetCtrl, Interpolator.EASE_BOTH);
        
        Timeline t = new Timeline(
                new KeyFrame(Duration.seconds(timeMove * 2), moveTo2, cubicTo2, ctrlX1_2, ctrlX2_2)
        );
        
       return t; 
    }
    
    private Circle getPupil(int i, double centerX, double centerY, double r)
    {
        Circle circle = new Circle(i * centerX, centerY, r);
        circle.setFill(Color.BLACK);
        
        return circle;
    }
    
    private Path getEye(double i, double startX, double endX, double eyeY, double eyeH)
    {
        Path path = new Path();
        path.setStroke(Color.BLACK);
        
        startX *= i;
        endX *= i;
        
        MoveTo offset = new MoveTo();
        offset.setX(startX);
        offset.setY(eyeY);
        
        CubicCurveTo curve = new CubicCurveTo();
        curve.setX(endX);
        curve.setY(eyeY);
        curve.setControlX1(offset.getX() + 5*i);
        curve.setControlY1(eyeY - eyeH);
        curve.setControlX2(curve.getX() - 5*i);
        curve.setControlY2(eyeY - eyeH);
        
        ClosePath closePath = new ClosePath();
        path.getElements().addAll(offset, curve, closePath);
        
        return path;
    }
    
    private Path getFrontEye(int i, double startX, double endX, double eyeY, double eyeH, double ctrlX1, double ctrlX2)
    {
        Path path = new Path();
        path.setStroke(Color.BLACK);
        
        int index = i == 0 ? -1 : 1;
        startX *= index;
        endX *= index;
        
        MoveTo offset = new MoveTo();
        offset.setX(startX);
        offset.setY(eyeY);
        eyeFrontMoveTo[i] = offset;
        
        CubicCurveTo curve = new CubicCurveTo();
        curve.setX(endX);
        curve.setY(eyeY);
        curve.setControlX1(ctrlX1 * index);
        curve.setControlY1(eyeY - eyeH);
        curve.setControlX2(ctrlX2 * index);
        curve.setControlY2(eyeY - eyeH);
        eyeFrontCurveTo[i] = curve;
        
        ClosePath closePath = new ClosePath();
        path.getElements().addAll(offset, curve, closePath);
        
        return path;
    }

    public void setRandomMouthColor() {
        mouthColor = mouthNextColor;
        mouthNextColor = colors[(int) (Math.random() * colors.length)];
        mouth.setFill(mouthColor);
        mouthNext.setFill(mouthNextColor);
    }

    public Color getMouthColor() {
        return mouthColor;
    }

    public void onMouseMoved(MouseEvent event) {
        becomeActive();
        double x = event.getX() * Main.WINDOW_WIDTH / Main.SCREEN_WIDTH;
        double y = event.getY() * Main.WINDOW_HEIGHT / Main.SCREEN_HEIGHT;
        double dx = getTranslateX() - x, dy = y - getTranslateY();
        double alpha = 90 - Math.toDegrees(Math.atan(dy / dx));
        if (x > getTranslateX()) {
            alpha -= 180;
        }
        setRotate(alpha);

        lastX = x;
        lastY = y;

        if(Main.instance.gameState == GameState.LASER_BONUS)
        {
            Game.instance.laserController.setLineEndPoint(getTranslateX(), getTranslateY(), x, y);
        }
    }

    public void update()
    {
        if(inactive == false && ++ticksInactive > TICKS_LIMIT)
        {
            becomeInactive();
        }

        if(inactive)
        {
            sunZController.update();
        }
    }

    public void becomeInactive() {
        inactive = true;
        for(int i = 0; i < 2; i++)
        {
            getChildren().add(eyesFront[i]);
            sunAnimation[i].play();
        }
    }
    
    //called onClick, onMouseMove
    public void becomeActive() {
        ticksInactive = 0;
        if(inactive)
        {
            for(int i = 0; i < 2; i++)
            { 
                getChildren().remove(eyesFront[i]);
                sunAnimation[i].stop();
                eyeFrontMoveTo[i].setY(eyeFrontStartY);
                eyeFrontCurveTo[i].setY(eyeFrontStartY);
                int index = i == 0 ? -1 : 1;
                eyeFrontCurveTo[i].setControlX1(eyeFrontStartCtrlX1 * index);
                eyeFrontCurveTo[i].setControlX2(eyeFrontStartCtrlX2 * index);
            }
            
            inactive = false;
            removeZ();       
        }
    }
    
    //called onGameOver, onClick, onMouseMove
    public void removeZ() {
        sunZController.makeInactive();
    }

    public void activateLine() {
        Game.instance.laserController.setLineEndPoint(getTranslateX(), getTranslateY(), lastX, lastY);
    }

    public void addClouds() {
        for(Cloud cloud : clouds) {
            cloud.activate();
        }
    }

    public void removeClouds() {
        for(Cloud cloud : clouds) {
            cloud.deactivate(root);
        }
    }

    public Cloud[] getClouds() {
        return clouds;
    }
}
