/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.scene.Group;
import objects.Z;

/**
 *
 * @author Korisnik
 */
public class ZController extends Group {
    
    private List<Z> moonSleep = new ArrayList<>();
    private int moonZActiveIndex = 0;
    private int moonZSize = 5;
    private int ticksToActivate = 60;
    private Random rand = new Random();

    public ZController(Group root, double x, double y)
    {
        for(int i = 0; i < moonZSize; i++)
        {
            Z moonZ = new Z(x, y);
            moonSleep.add(moonZ);
            root.getChildren().add(moonZ);
        }  
    }
        
    public void update()
    {
        if(--ticksToActivate == 0)
        {
            ticksToActivate = 30 + rand.nextInt(60 * 3);
            moonSleep.get(moonZActiveIndex).makeActive();
            moonZActiveIndex = (moonZActiveIndex + 1) % moonZSize;         
        }
        
        for(Z z : moonSleep) z.update();
    }
    
    public void makeInactive()
    {
        for(Z z : moonSleep) z.makeInactive();
    }
}
