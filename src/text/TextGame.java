/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Korisnik
 */
public class TextGame extends Group {
    
    public static final Font TEXT_FONT = Font.font("Tahoma", 30);
    private Text text;
    
    public TextGame(double x, double y, String str, Color fillColor)
    {
        text = new Text(x, y, str);
        text.setFill(fillColor);
        text.setFont(TEXT_FONT);
        text.setStrokeWidth(1.25);
        text.setStroke(Color.WHITE);
        getChildren().addAll(text);
    }
    
    public void setText(String str) {
        text.setText(str);
    }

    public void update() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
