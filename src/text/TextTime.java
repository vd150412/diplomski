/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text;

import javafx.scene.paint.Color;

/**
 *
 * @author Korisnik
 */
public class TextTime extends TextGame {
    
    private int ticks = 0;
    private int seconds = 0;
    
    public TextTime(double x, double y, String str, Color fillColor) {
        super(x, y, str, fillColor);
    }
    
    public void updateTime()
    {
        if(++ticks == 60)
        {
            seconds++;
            ticks = 0;
            setText("Time: " + seconds);
        }
    }

    public int getTime() {
        return seconds;
    }
}
