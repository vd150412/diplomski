/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text;

import javafx.scene.paint.Color;

/**
 *
 * @author dvucinic
 */
public class TextScore extends TextGame {
    
    private int score = 0;
    
    public TextScore(double x, double y, String str, Color fillColor) {
        super(x, y, str, fillColor);
    }
    
    public void updateScore(int scoreToAdd)
    {
        score += scoreToAdd;
        setText("Score: " + score);
    }

    public int getScore() {
        return score;
    }
}
