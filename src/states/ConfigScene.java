package states;

import controllers.Config;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import main.Main;
import objects.UI.BackIcon;
import objects.UI.GameIcon;

import java.io.File;

import static controllers.Config.SETTINGS_ITEMS;

public class ConfigScene extends GameMenu implements StateController {

    private static final Stop[] stops = new Stop []{
            new Stop(0, Color.LIGHTSEAGREEN),
            new Stop(0.8, Color.LIGHTBLUE),
            new Stop(1, Color.LIGHTSEAGREEN)
    };

    private static final LinearGradient BACKGROUND_FILL = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
    protected static final Font FONT_SETTINGS_TEXTS = Font.font("Courier New", 18);

    private static final String[] MENU_TEXTS = {
            "OK"
    };

    private Text[] texts = new Text[SETTINGS_ITEMS.length];

    public ConfigScene()
    {
        super("", MENU_TEXTS, BACKGROUND_FILL, Main.WINDOW_HEIGHT * 0.85);

        double textX = 50;
        for(int i = 0; i < SETTINGS_ITEMS.length; i++)
        {
            double rectY = 50 + 38 * i;

            Config.SettingsItem item = SETTINGS_ITEMS[i];
            String str = "[" + item.minValue + " - " + item.maxValue + "]\t" + item.settingsText + " : " + item.value;

            Text text = new Text(textX, rectY, str);
            text.setFont(FONT_SETTINGS_TEXTS);
            text.setFill(Color.WHITE);
            text.setX(textX);
            text.setTextAlignment(TextAlignment.JUSTIFY);
            text.setStyle("-fx-font-weight: bold");
            texts[i] = text;

            root.getChildren().addAll(text);
        }
    }

    @Override
    public void onKeyPressed(KeyEvent event)
    {
        super.onKeyPressed(event);

        if(event.getCode() == KeyCode.ESCAPE)
        {
            Main.instance.fromConfig();
        }
    }

    @Override
    public void menuItemAction() {
        Main.instance.fromConfig();
    }
}
