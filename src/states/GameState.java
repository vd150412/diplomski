package states;

public enum GameState {
    GAME,
    LASER_BONUS,
    GAME_OVER
};