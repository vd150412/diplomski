package states;

import controllers.*;
import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.util.Pair;
import main.*;
import objects.*;
import objects.UI.PauseIcon;
import objects.balls.*;
import objects.picks.CoinController;
import objects.picks.LaserPickController;
import text.TextScore;
import text.TextTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.scene.input.MouseEvent;

public class Game implements StateController {

    public Group root;
    public Moon moon;
    public Sun sun;

    public TextTime textTime;
    public TextScore textScore;
    public PauseIcon pauseIcon;

    public List<Ball> balls = new ArrayList<>();
    public List<Shot> shots = new ArrayList<>();

    public int ballsCreated = 1;
    public boolean isCreatedLevel;

    public CoinController coinController;
    public LaserPickController laserPickController;
    public StarController starController;
    public BallController ballController;
    public CrashController crashController;
    public LaserController laserController;
    public ShotMissController shotMissController;

    public static Game instance;

    public Game(boolean isCreatedLevel, ArrayList<Line> lines, Pair<Double, Double> sunPosition)
    {
        instance = this;

        this.root = new Group();
        this.isCreatedLevel = isCreatedLevel;

        Background background = new Background(Main.WINDOW_WIDTH, Main.WINDOW_HEIGHT);
        root.getChildren().addAll(background);

        ballController = new BallController(balls);
        shotMissController = new ShotMissController();
        LevelController levelController = isCreatedLevel ?
                new LevelController(root, lines, sunPosition.getKey(), sunPosition.getValue()) :
                new LevelController(root);

        sun = new Sun(levelController.sunX, levelController.sunY, root);
        moon = new Moon(levelController.getLastLine().getEndX(), levelController.getLastLine().getEndY(), root);

        final double pauseX = Main.WINDOW_WIDTH - PauseIcon.ICON_RADIUS * 2;
        final double y = 40;
        textTime = new TextTime(pauseX - 370, y, "Time: 0", Color.WHITE);
        textScore = new TextScore(pauseX - 200, y, "Score: 0", Color.WHITE);
        pauseIcon = new PauseIcon(pauseX, y, Color.color(1, 1, 1, 0.1));

        coinController = new CoinController(root);
        laserPickController = new LaserPickController(root);
        starController = new StarController(root);
        crashController = new CrashController();
        laserController = new LaserController(levelController.sunX, levelController.sunY);
        balls.add(new Ball());

        root.getChildren().addAll(moon, textTime, textScore, pauseIcon);
        root.getChildren().addAll(laserController.getLaserLine(), sun, balls.get(0));
        root.getChildren().addAll(sun.getClouds());
    }

    public Game()
    {
        this(false, null, null);
    }

    @Override
    public void onMouseMoved(MouseEvent event) {
        sun.onMouseMoved(event);
    }

    @Override
    public void onMouseClicked(MouseEvent event) {
        boolean allDestroyed = balls.size() == 0 && ballsCreated == Config.MAX_BALLS;
        boolean pauseClicked = event.getPickResult().getIntersectedNode().getParent() == pauseIcon;
        if(Main.instance.gameState != GameState.GAME_OVER && !allDestroyed && !pauseClicked)
        {
            makeShot(new Shot(sun));
            sun.setRandomMouthColor();
            sun.becomeActive();
        }
    }

    @Override
    public void onKeyPressed(KeyEvent event) {
        if(event.getCode() == KeyCode.ESCAPE)
        {
            Main.instance.changeState(Main.instance.pauseMenu);
        }
    }

    @Override
    public Group getRoot() {
        return root;
    }

    @Override
    public void update()
    {
        if(Main.instance.gameState == GameState.GAME_OVER) return;

        if (!balls.isEmpty() && balls.get(0).getBoundsInParent().intersects(moon.getMoonBounds())) {
            onGameOver();
        }

        if((balls.size() > 0 && balls.get(balls.size() - 1).canAddBall())
                || (ballsCreated < Config.MAX_BALLS && balls.size() == 0))
        {
            if(ballsCreated < Config.MAX_BALLS)
            {
                Ball ball = getNextBall();
                balls.add(ball);
                root.getChildren().add(ball);
                ballsCreated++;
            }
        }

        balls.forEach(ball -> ball.update());
        moon.update();
        sun.update();
        coinController.update(shots);
        laserPickController.update(shots);
        starController.update();
        ballController.update();
        crashController.update();
        shotMissController.update();
        //if(Main.instance.gameState == GameState.LASER_BONUS) laserController.update();

        if(ballsCreated == Config.MAX_BALLS && balls.size() == 0)
        {
            Main.instance.levelComplete();
        }

        textTime.updateTime();
    }

    private void onGameOver()
    {
        for(Shot shot : shots)
        {
            root.getChildren().remove(shot);
        }
        shots.clear();
        Main.instance.gameState = GameState.GAME_OVER;
        moon.onGameOver();
        sun.removeZ();
    }

    public void makeShot(Shot shot) {
        shots.add(shot);
        root.getChildren().add(shot);
    }

    private Random rand = new Random();
    public Ball getNextBall()
    {
        double num = rand.nextDouble();

        if(num < Config.RAND_BALL_BOMB) return new Ball();
        else if(num < Config.RAND_BALL_SHIELD) return new BallBomb();
        else if(num < Config.RAND_BALL_PAUSE) return new BallShield();
        else if(num < Config.RAND_BALL_REVERSE) return new BallPause();
        else return new BallReverse();
    }
}
