package states;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import main.Main;

public abstract class GameMenu extends Group implements StateController {

    protected static final double RECT_WIDTH = Main.WINDOW_WIDTH / 3;
    protected static final double RECT_HEIGHT = 65;

    protected static final double MENU_ITEM_DISTANCE = 25;
    protected static final double MENU_ITEM_ARC = 20;
    protected static final Font MENU_ITEM_FONT = Font.font("Courier New", 40);
    protected static final Font TITLE_FONT = Font.font("Courier New", 50);

    public static final Color MENU_ITEM_STROKE_COLOR = Color.color(0.64, 0.97, 0.1);

    protected Group root;
    protected int selectedIndex;
    protected int numOfMenuItems;
    protected String[] menuTexts;
    protected Rectangle[] rects;

    public GameMenu(String title, String[] menuTexts, Paint backgroundFill) {
        this(title, menuTexts, backgroundFill, (Main.WINDOW_HEIGHT - MENU_ITEM_DISTANCE * (menuTexts.length - 1) - RECT_HEIGHT * menuTexts.length) / 2);
    }

    public GameMenu(String title, String[] menuTexts, Paint backgroundFill, double margin)
    {
        this.numOfMenuItems = menuTexts.length;
        this.menuTexts = menuTexts;
        this.rects = new Rectangle[numOfMenuItems];

        Rectangle background = new Rectangle(0, 0, Main.WINDOW_WIDTH + 10, Main.WINDOW_HEIGHT + 10);
        background.setFill(backgroundFill);

        root = new Group();
        root.getChildren().add(background);

        Text titleText = new Text(Main.WINDOW_WIDTH / 2, Main.WINDOW_HEIGHT * 0.15, title);
        titleText.setFont(TITLE_FONT);
        titleText.setFill(Color.WHITE);
        titleText.setX(Main.WINDOW_WIDTH / 2 - titleText.getBoundsInParent().getWidth() / 2);
        titleText.setTextAlignment(TextAlignment.JUSTIFY);
        titleText.setStyle("-fx-font-weight: bold");
        root.getChildren().addAll(titleText);

        double rectX = Main.WINDOW_WIDTH / 2 - RECT_WIDTH / 2;
        double textX = Main.WINDOW_WIDTH / 2;

        for(int i = 0; i < numOfMenuItems; i++)
        {
            double rectY = margin + i * MENU_ITEM_DISTANCE + i * RECT_HEIGHT;

            Rectangle rect = new Rectangle(rectX, rectY, RECT_WIDTH, RECT_HEIGHT);
            final double gray = 0.35;
            rect.setFill(Color.color(gray, gray, gray));
            rect.setStroke(Color.WHITE);
            rect.setStrokeWidth(10);
            rect.setArcHeight(MENU_ITEM_ARC);
            rect.setArcWidth(MENU_ITEM_ARC);
            rects[i] = rect;

            Text text = new Text(textX, rectY, menuTexts[i]);
            text.setFont(MENU_ITEM_FONT);
            text.setFill(Color.WHITE);
            text.setX(textX - text.getBoundsInParent().getWidth() / 2);
            text.setY(rectY + RECT_HEIGHT / 2 + text.getBoundsInParent().getHeight() / 4);
            text.setTextAlignment(TextAlignment.JUSTIFY);
            text.setStyle("-fx-font-weight: bold");

            Group menuItem = new Group();
            menuItem.getChildren().addAll(rect, text);

            root.getChildren().addAll(menuItem);
        }

        rects[0].setStroke(MENU_ITEM_STROKE_COLOR);
    }

    public abstract void menuItemAction();

    @Override
    public void onMouseMoved(MouseEvent event) {
        if(event.getPickResult() == null || event.getPickResult().getIntersectedNode() == null) return;

        Node node = event.getPickResult().getIntersectedNode().getParent();
        boolean menuItemSelected = false;
        int menuItemIndex = 0;

        for(int i = 0; i < rects.length; i++)
        {
            if(rects[i].getParent() == node)
            {
                menuItemIndex = i;
                menuItemSelected = true;
                break;
            }
        }

        if(menuItemSelected && selectedIndex != menuItemIndex)
        {
            rects[selectedIndex].setStroke(Color.WHITE);
            rects[menuItemIndex].setStroke(MENU_ITEM_STROKE_COLOR);
            selectedIndex = menuItemIndex;
        }
    }

    @Override
    public void onMouseClicked(MouseEvent event) {
        Node node = event.getPickResult().getIntersectedNode().getParent();
        boolean menuItemSelected = false;
        for(int i = 0; i < rects.length; i++)
        {
            if(rects[i].getParent() == node)
            {
                selectedIndex = i;
                menuItemSelected = true;
                break;
            }
        }

        if(menuItemSelected)
        {
            menuItemAction();
        }
    }

    @Override
    public void onKeyPressed(KeyEvent event)
    {
        KeyCode code = event.getCode();
        boolean up = code == KeyCode.UP || code == KeyCode.W;
        boolean down = code == KeyCode.DOWN || code == KeyCode.S;

        if(up || down)
        {
            rects[selectedIndex].setStroke(Color.WHITE);

            if(down)
            {
                selectedIndex = (selectedIndex + 1) % numOfMenuItems;
            }
            else if(up)
            {
                selectedIndex --;
                if(selectedIndex == -1) selectedIndex = numOfMenuItems - 1;
            }

            rects[selectedIndex].setStroke(MENU_ITEM_STROKE_COLOR);
            return;
        }

        if(code == KeyCode.ENTER)
        {
            menuItemAction();
        }
    }

    @Override
    public Group getRoot() {
        return root;
    }

    @Override
    public void update() {

    }
}
