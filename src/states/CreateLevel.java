package states;

import controllers.Config;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.util.Pair;
import main.Main;
import objects.Background;
import objects.Moon;
import objects.Sun;
import objects.UI.*;
import objects.balls.Ball;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;

public class CreateLevel implements StateController, EventHandler<Event> {

    @Override
    public void handle(Event event)
    {
        if(event instanceof MouseEvent) mouseEvent((MouseEvent) event);
    }

    private enum State { BALL_ADD, BALL_MODIFY, SUN_SELECTED, ICON_COLLISION, MOON_SELECTED};
    private State state = State.BALL_ADD;

    private Group root;
    private Ball currentBall;
    private int selectedIndex;

    private ArrayList<Line> lines = new ArrayList<>();
    private ArrayList<Ball> balls = new ArrayList<>();

    public Sun sun;
    private boolean ctrlActive = false;
    private static final double CTRL_DISTANCE = 50;
    private int lineFoundIndex;

    private double eventX, eventY;
    private static final Color LINE_COLOR = Color.YELLOW;
    private static final Color LINE_SELECTED_COLOR = Color.ORANGE;

    private boolean moonAdded;
    private Moon moon;

    private ArrayList<ButtonIcon> buttons = new ArrayList<>();
    private ButtonIcon buttonSelected;

    private Pair[] buttonActions = new Pair[]{
            new Pair<String, EventHandler<MouseEvent>>("PLAY", e -> {
                startGame();
            }),
            new Pair<String, EventHandler<MouseEvent>>("APPLY", e -> {
                addMoon();
            }),
            new Pair<String, EventHandler<MouseEvent>>("SAVE", e -> {
                saveLevel();
            }),
            new Pair<String, EventHandler<MouseEvent>>("SAVE AS", e -> {
                saveAsLevel();
            }),
            new Pair<String, EventHandler<MouseEvent>>("CLEAR", e -> {
                deleteElements();
            }),
            new Pair<String, EventHandler<MouseEvent>>("CANCEL", e -> {
                toStartMenu();
            })
    };

    public CreateLevel()
    {
        this.root = new Group();
        root.setOnMouseDragged(this);
        root.setOnMousePressed(this);

        sun = new Sun(Main.WINDOW_WIDTH / 2, Main.WINDOW_HEIGHT / 2, root);
        moon = new Moon(0, 0, root);
        Background bg = new Background(Main.WINDOW_WIDTH, Main.WINDOW_HEIGHT);
        root.getChildren().addAll(bg, sun);

        for(int i = 0; i < buttonActions.length; i++)
        {
            Pair<String, EventHandler<MouseEvent>> pair = buttonActions[i];

            double buttonY = Main.WINDOW_HEIGHT
                    - ButtonIcon.RECT_HEIGHT * (buttonActions.length + 1) * 1.5
                    + ButtonIcon.RECT_HEIGHT * (i + 1) * 1.5;

            ButtonIcon button = new ButtonIcon(pair.getKey(), Main.WINDOW_WIDTH - ButtonIcon.RECT_WIDTH * 1.25, buttonY, Color.WHITE, pair.getValue());
            buttons.add(button);
            root.getChildren().add(button);
        }
    }

    private void ballAdd(MouseEvent event, double x, double y)
    {
        if(event.getEventType() == MouseEvent.MOUSE_PRESSED)
        {
            if(ctrlActive && lineFoundIndex != -1)
            {
                addBallCtrl(x, y);
                return;
            }

            if(!moonAdded)
            {
                addBallToTheEnd(x, y);
            }
        }
    }

    private void addBallToTheEnd(double x, double y)
    {
        if(balls.size() == 0)
        {
            currentBall = selectBall(new Ball(x, y));
            balls.add(currentBall);
            root.getChildren().add(currentBall);
        }
        else
        {
            Ball lastBall = balls.get(balls.size() - 1);

            Line lastLine = new Line();
            lastLine.setStartX(lastBall.getTranslateX());
            lastLine.setStartY(lastBall.getTranslateY());
            lastLine.setEndX(x);
            lastLine.setEndY(y);
            lastLine.setStrokeWidth(10);
            lastLine.setStroke(LINE_COLOR);
            lines.add(lastLine);

            currentBall = selectBall(new Ball(x, y));
            balls.add(currentBall);
            root.getChildren().add(currentBall);
            root.getChildren().add(root.getChildren().size() - balls.size(), lastLine);
        }

        state = State.BALL_MODIFY;
        selectedIndex = balls.size() - 1;
    }

    private void addBallCtrl(double x, double y) {
        Line lineTarget = lines.get(lineFoundIndex);
        lines.remove(lineFoundIndex);

        Line line1 = new Line();
        line1.setStartX(lineTarget.getStartX());
        line1.setStartY(lineTarget.getStartY());
        line1.setEndX(x);
        line1.setEndY(y);
        line1.setStrokeWidth(10);
        line1.setStroke(LINE_COLOR);
        lines.add(lineFoundIndex, line1);

        Line line2 = new Line();
        line2.setStartX(x);
        line2.setStartY(y);
        line2.setEndX(lineTarget.getEndX());
        line2.setEndY(lineTarget.getEndY());
        line2.setStrokeWidth(10);
        line2.setStroke(LINE_COLOR);
        lines.add(lineFoundIndex + 1, line2);

        currentBall = selectBall(new Ball(x, y));
        balls.add(lineFoundIndex + 1, currentBall);
        root.getChildren().add(currentBall);
        int lineIndex = root.getChildren().size() - balls.size() - (moonAdded ? 1 : 0);
        root.getChildren().add(lineIndex, line1);
        root.getChildren().add(lineIndex, line2);
        root.getChildren().removeAll(lineTarget);

        state = State.BALL_MODIFY;
        selectedIndex = lineFoundIndex + 1;
    }

    private void ballModify(MouseEvent event, double x, double y)
    {
        if(event.getEventType() == MouseEvent.MOUSE_DRAGGED || (moonAdded && selectedIndex == balls.size() - 1))
        {
            currentBall.setTranslateX(x);
            currentBall.setTranslateY(y);

            int line1Index = selectedIndex - 1;
            int line2Index = selectedIndex;

            if(line1Index >= 0)
            {
                Line line = lines.get(line1Index);
                line.setEndX(x);
                line.setEndY(y);
            }

            if(line2Index < lines.size())
            {
                Line line = lines.get(line2Index);
                line.setStartX(x);
                line.setStartY(y);
            }
        }
    }

    private void removeSelectedBall()
    {
        if(selectedIndex != -1 && !(moonAdded && selectedIndex == balls.size() - 1))
        {
            root.getChildren().remove(currentBall);
            balls.remove(selectedIndex);

            int lineIndex1 = selectedIndex - 1;
            int lineIndex2 = selectedIndex;

            if(lineIndex1 >= 0)
            {
                Line line1 = lines.get(lineIndex1);
                root.getChildren().remove(line1);

                if(lineIndex2 < lines.size())
                {
                    Line line2 = lines.get(lineIndex2);
                    line2.setStartX(line1.getStartX());
                    line2.setStartY(line1.getStartY());
                }

                lines.remove(line1);
            }
            else//deleting first ball;
            {
                Line firstLine = lines.get(0);
                root.getChildren().remove(firstLine);
                lines.remove(firstLine);
            }

            currentBall = null;
            selectedIndex = -1;
        }
    }

    private void removeLastBall()
    {
        if(balls.size() > 0 && !moonAdded)
        {
            Ball lastBall = balls.get(balls.size() - 1);
            balls.remove(balls.size() - 1);
            root.getChildren().remove(lastBall);
            if(lastBall == currentBall) {
                currentBall = null;
                selectedIndex = -1;
            }

            if(lines.size() > 0)
            {
                Line lastLine = lines.get(lines.size() - 1);
                lines.remove(lines.size() - 1);
                root.getChildren().remove(lastLine);
            }
        }
    }

    private void sunSelected(double x, double y) {
        sun.setTranslateX(x);
        sun.setTranslateY(y);
    }

    private void moonSelected(MouseEvent event, double x, double y) {
        moon.setTranslateX(x);
        moon.setTranslateY(y);

        selectedIndex = balls.size() - 1;
        currentBall = selectBall(balls.get(selectedIndex));
        ballModify(event, x, y);
    }

    private void mouseEvent(MouseEvent event)
    {
        double x = event.getX();
        double y = event.getY();
        if(event.getEventType() == MouseEvent.MOUSE_PRESSED)
        {
            ButtonIcon button = buttonPressed(event);
            if(button != null)
            {
                button.handle(event);
                state = State.ICON_COLLISION;
                return;
            }
            else if(checkSelected(event, sun))
            {
                state = State.SUN_SELECTED;
            }
            else if(checkSelected(event, moon))
            {
                state = State.MOON_SELECTED;
            }
            else if (checkSelectedNode(event))
            {
                currentBall = selectBall(balls.get(selectedIndex));
                state = State.BALL_MODIFY;
            }
            else
            {
                state = State.BALL_ADD;
            }
        }

        if(state == State.BALL_ADD)
        {
            ballAdd(event, x, y);
        }
        else if(state == State.BALL_MODIFY)
        {
            ballModify(event, x, y);
        }
        else if(state == State.SUN_SELECTED)
        {
            sunSelected(x, y);
        }
        else if(state == State.MOON_SELECTED)
        {
            moonSelected(event, x, y);
        }
    }

    private boolean checkSelectedNode(MouseEvent event) {
        Node node = event.getPickResult().getIntersectedNode().getParent();

        for(int i = 0; i < balls.size(); i++)
        {
            Ball ball = balls.get(i);
            if(ball == node)
            {
                selectedIndex = i;
                return true;
            }
        }

        return false;
    }

    private boolean checkSelected(MouseEvent event, Group object) {
        Node node = event.getPickResult().getIntersectedNode();

        if(node.getParent() == object)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private ButtonIcon buttonPressed(MouseEvent event)
    {
        for(ButtonIcon button : buttons)
        {
            Node node = event.getPickResult().getIntersectedNode();
            if(button.intersects(node))
            {
                return button;
            }
        }

        return null;
    }

    @Override
    public void update()
    {

    }

    private void updateLines()
    {
        double minDist = Integer.MAX_VALUE;
        Line lineFound = null;

        for(int i = 0; i < lines.size(); i++)
        {
            Line line = lines.get(i);

            double minX = Math.min(line.getStartX(), line.getEndX());
            double maxX = Math.max(line.getStartX(), line.getEndX());
            double minY = Math.min(line.getStartY(), line.getEndY());
            double maxY = Math.max(line.getStartY(), line.getEndY());

            double percentage = 0.1;
            double diffX = maxX - minX;
            double diffY = maxY - minY;

            minX += diffX * percentage;
            maxX -= diffX * percentage;

            minY += diffY * percentage;
            maxY -= diffY * percentage;

            if(eventX >= minX && eventX <= maxX && eventY + CTRL_DISTANCE > minY && eventY - CTRL_DISTANCE < maxY ||
                    eventY >= minY && eventY <= maxY && eventX + CTRL_DISTANCE > minX && eventX - CTRL_DISTANCE < maxX)
            {
                double dist = getDistance(line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY(), eventX, eventY);
                if(dist < CTRL_DISTANCE && dist < minDist)
                {
                    minDist = dist;
                    lineFound = line;
                }
            }
        }

        lineFoundIndex = -1;
        for(int i = 0; i < lines.size(); i++)
        {
            Line line = lines.get(i);

            if(line == lineFound)
            {
                line.setStroke(LINE_SELECTED_COLOR);
                lineFoundIndex = i;
            }
            else
            {
                line.setStroke(LINE_COLOR);
            }
        }
    }

    private double getDistance(double lineX1, double lineY1, double lineX2, double lineY2, double x, double y) {
        double up = (lineX2 - lineX1) * (y - lineY1) - (lineY2 - lineY1) * (x - lineX1);
        double down = (lineY2 - lineY1) * (lineY2 - lineY1) + (lineX2 - lineX1) * (lineX2 - lineX1);

        up = Math.abs(up);
        down = Math.sqrt(down);
        return up / down;
    }

    @Override
    public void onMouseMoved(MouseEvent event) {
        eventX = event.getX();
        eventY = event.getY();

        double x = event.getX() * Main.WINDOW_WIDTH / Main.SCREEN_WIDTH;
        double y = event.getY() * Main.WINDOW_HEIGHT / Main.SCREEN_HEIGHT;

        eventX = x;
        eventY = y;

        if(ctrlActive) {
            updateLines();
        }

        ButtonIcon buttonIcon = buttonPressed(event);
        if(buttonIcon != buttonSelected)
        {
            if(buttonSelected != null) buttonSelected.deselect();
            buttonSelected = buttonIcon;
            if(buttonSelected != null) buttonSelected.select();
        }
    }

    @Override
    public void onMouseClicked(MouseEvent event) {
    }

    @Override
    public void onKeyPressed(KeyEvent event)
    {
        if(event.getCode() == KeyCode.CONTROL)
        {
            ctrlActive = true;
            updateLines();
        }
        else if(event.getCode() == KeyCode.BACK_SPACE)
        {
            removeLastBall();
        }
        else if(event.getCode() == KeyCode.DELETE)
        {
            removeSelectedBall();
        }
        else if(event.getCode() == KeyCode.ENTER)
        {
            addMoon();
        }
    }

    @Override
    public void onKeyReleased(KeyEvent event)
    {
        if(event.getCode() == KeyCode.CONTROL)
        {
            ctrlActive = false;
            for(Line line : lines)
            {
                line.setStroke(LINE_COLOR);
            }
        }
    }

    private boolean startGame() {
        if(lines.size() > 0)
        {
            Main.instance.toGameFromCreateLevel();
            return true;
        }
        else
        {
            return false;
        }
    }

    private void toStartMenu() {
        deleteElements();
        Main.instance.toStartMenu();
    }

    private void deleteElements() {
        root.getChildren().removeAll(lines);
        root.getChildren().removeAll(balls);
        lines.clear();
        balls.clear();

        if(moonAdded)
        {
            root.getChildren().remove(moon);
        }

        moonAdded = false;
        currentBall = null;
        selectedIndex = -1;
    }

    public ArrayList<Line> getLines() {
        return lines;
    }

    public Pair<Double, Double> getSunPosition() {
        return new Pair<>(sun.getTranslateX(), sun.getTranslateY());
    }

    @Override
    public Group getRoot() {
        return root;
    }

    private Ball selectBall(Ball ball) {
        if(currentBall != null) currentBall.deselectBall();
        currentBall = ball;
        currentBall.selectBall();
        return ball;
    }

    private void addMoon()
    {
        if(balls.size() >= 2 && !moonAdded)
        {
            Ball lastBall = balls.get(balls.size() - 1);
            moon.setTranslateX(lastBall.getTranslateX());
            moon.setTranslateY(lastBall.getTranslateY());
            root.getChildren().add(moon);

            if(currentBall == lastBall) {
                currentBall = null;
                selectedIndex = -1;
            }

            moonAdded = true;
        }
    }

    private void saveLevel()
    {
        if(balls.size() < 2) return;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Save path");
        alert.setHeaderText(null);
        alert.setContentText("You will lose current rankings. Are you ok with this?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK)
        {
            saveLevel(new File(Config.CONFIG_FILE_PATH), true);
        }
    }

    private void saveAsLevel()
    {
        if(balls.size() < 2) return;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(Config.getFileChooserDir());
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle("Save path to Config file");

        String initName = Config.CONFIG_FILE_NAME.substring(0, Config.CONFIG_FILE_NAME.length() - 5) + "_new_path.json";
        fileChooser.setInitialFileName(initName);
        File selectedFile = fileChooser.showSaveDialog(Main.instance.primaryStage);
        if(selectedFile != null && !selectedFile.isDirectory())
        {
            Config.FILE_CHOOSER_DIR = selectedFile.getParentFile();
        }

        if(selectedFile == null) return;

        if(selectedFile.getAbsolutePath().equals(Config.CONFIG_FILE_PATH))
        {
            saveLevel(selectedFile, true);
        }
        else
        {
            saveLevel(selectedFile, false);
        }
    }

    private void saveLevel(File file, boolean currentConfigModified)
    {
        boolean error = Config.SAVE_JSON(file, lines, sun);

        if(error)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Settings");
            alert.setHeaderText(null);
            alert.setContentText("Error saving JSON file");
            alert.showAndWait();
            return;
        }

        if(currentConfigModified)
        {
            Config.LOAD_JSON(file);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Save path");
            alert.setHeaderText(null);
            alert.setContentText("Path is successfully saved!");
            alert.showAndWait();
            System.out.println("LOADING CURRENT FILE AGAIN!");
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Save config file");
            alert.setHeaderText("Config file is successfully saved!");
            alert.setContentText("Do you want to load saved config file?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK)
            {
                System.out.println("LOAD SAVED LEVEL!");
                boolean errorLoad = Config.LOAD_JSON(file);

                if(errorLoad) return;

                Alert alertLoad = new Alert(Alert.AlertType.INFORMATION);
                alertLoad.setTitle("Load config file");
                alertLoad.setHeaderText(null);
                alertLoad.setContentText("Config file is successfully loaded");
                alertLoad.showAndWait();
            }
        }
    }
}
