package states;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import main.Main;
import objects.UI.ButtonIcon;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class StartMenu extends GameMenu implements StateController{

    private static final String[] MENU_TEXTS = {
            "START GAME",
            "TOP LIST",
            "CREATE LEVEL",
            "SETTINGS",
            "ABOUT",
            "EXIT"
    };

    private static final Stop[] stops = new Stop []{
            new Stop(0, Color.LIGHTSEAGREEN),
            new Stop(0.3, Color.LIGHTBLUE),
            new Stop(1, Color.LIGHTSEAGREEN)
    };

    private static final LinearGradient BACKGROUND_FILL = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);

    private static final int START_GAME = 0;
    private static final int TOP_LIST = 1;
    private static final int CREATE_LEVEL = 2;
    private static final int SETTINGS = 3;
    private static final int ABOUT = 4;
    private static final int EXIT = 5;

    private static final String HELP_FILE_NAME = "uputstvo.pdf";
    private static ButtonIcon helpButton;
    private boolean helpButtonSelected = false;

    public StartMenu()
    {
        super("", MENU_TEXTS, BACKGROUND_FILL);

        helpButton = new ButtonIcon("HELP", Main.WINDOW_WIDTH - ButtonIcon.RECT_WIDTH - 20, 20, Color.color(1, 1, 1), e ->{
            if (Desktop.isDesktopSupported())
            {
                try
                {
                    File myFile = new File(HELP_FILE_NAME);
                    Desktop.getDesktop().open(myFile);
                }
                catch (IOException ex)
                {
                   ex.printStackTrace();
                }
            }
        });
        root.getChildren().add(helpButton);
    }

    @Override
    public void onMouseMoved(MouseEvent event) {
        super.onMouseMoved(event);
        if(helpButton.intersects(event.getPickResult().getIntersectedNode()))
        {
            if(!helpButtonSelected)
            {
                helpButton.select();
                helpButtonSelected = true;
            }
        }
        else if(helpButtonSelected)
        {
            helpButtonSelected = false;
            helpButton.deselect();
        }
    }


    @Override
    public void onMouseClicked(MouseEvent event) {
        super.onMouseClicked(event);
        if(helpButton.intersects(event.getPickResult().getIntersectedNode()))
        {
            helpButton.handle(event);
        }
    }

    @Override
    public void menuItemAction()
    {
        if(selectedIndex == START_GAME)
        {
            Main.instance.changeState(Main.instance.game);
        }
        else if(selectedIndex == TOP_LIST)
        {
            Main.instance.toTopList();
        }
        else if(selectedIndex == CREATE_LEVEL)
        {
            Main.instance.toCreateLevel();
        }
        else if(selectedIndex == SETTINGS)
        {
            Main.instance.toSettings();
        }
        else if(selectedIndex == ABOUT)
        {
            Main.instance.toAbout();
        }
        else if(selectedIndex == EXIT)
        {
            Platform.exit();
        }
    }
}