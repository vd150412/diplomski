package states;

import controllers.TopListController;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Pair;
import main.Main;

import java.util.ArrayList;

public class LevelCompleteTopList extends GameMenu implements StateController {

    private static final double RECT_WIDTH = Main.WINDOW_WIDTH / 3;
    private static final double RECT_HEIGHT = 35;

    private static final double MENU_ITEM_DISTANCE = 2;
    private static final Font MENU_ITEM_FONT = Font.font("Courier New", 20);

    protected double RECT_CENTER_WIDTH = Main.WINDOW_WIDTH / 2.5;

    private static final String[] MENU_TEXTS = {
            "GO TO START MENU"
    };
    private static final int START_MENU = 0;

    public LevelCompleteTopList(int index, String[] texts, double margin)
    {
        super("TOP LIST", texts, Color.color(0, 0, 0, 0.65), margin);

        Rectangle background = new Rectangle(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2, 0, RECT_CENTER_WIDTH, Main.WINDOW_HEIGHT + 10);
        background.setFill( Color.color(0, 0, 0, 0.65));
        root.getChildren().add(1, background);

        ArrayList<TopListController.TopListEntry> topList = Main.instance.topListController.getTopList();
        int numOfMenuItems = topList.size();
        margin = Main.WINDOW_HEIGHT * 0.22;
        double rectX = Main.WINDOW_WIDTH / 2 - RECT_WIDTH / 2;
        double playerX = rectX + 25;
        double scoreX = rectX + RECT_WIDTH * 0.65;
        double timeX = rectX + RECT_WIDTH * 0.85;

        for(int i = 0; i < 10; i++)
        {
            String playerStr = i < numOfMenuItems ? topList.get(i).name : "";
            String scoreStr = i < numOfMenuItems ? topList.get(i).score + "" : "";
            String timeStr = i < numOfMenuItems ? topList.get(i).time + "s" : "";

            double rectY = margin + i * MENU_ITEM_DISTANCE + i * RECT_HEIGHT;

            Rectangle rect = new Rectangle(rectX, rectY, RECT_WIDTH, RECT_HEIGHT);
            final double gray = 0.35;
            rect.setFill(Color.color(gray, gray, gray));
            rect.setStroke(Color.WHITE);
            rect.setStrokeWidth(5);
            rect.setArcHeight(MENU_ITEM_ARC);
            rect.setArcWidth(MENU_ITEM_ARC);

            Color fillColor = index == i ? Color.RED : Color.WHITE;
            Text textPlayer = new Text(playerX, rectY, playerStr);
            textPlayer.setFont(MENU_ITEM_FONT);
            textPlayer.setFill(fillColor);
            textPlayer.setY(rectY + RECT_HEIGHT / 2 + textPlayer.getBoundsInParent().getHeight() / 4);
            textPlayer.setTextAlignment(TextAlignment.JUSTIFY);
            textPlayer.setStyle("-fx-font-weight: bold");

            Text textScore = new Text(scoreX, rectY, "" + scoreStr);
            textScore.setFont(MENU_ITEM_FONT);
            textScore.setFill(fillColor);
            textScore.setY(rectY + RECT_HEIGHT / 2 + textScore.getBoundsInParent().getHeight() / 4);
            textScore.setTextAlignment(TextAlignment.JUSTIFY);
            textScore.setStyle("-fx-font-weight: bold");

            Text textTime = new Text(timeX, rectY, timeStr);
            textTime.setFont(MENU_ITEM_FONT);
            textTime.setFill(fillColor);
            textTime.setY(rectY + RECT_HEIGHT / 2 + textScore.getBoundsInParent().getHeight() / 4);
            textTime.setTextAlignment(TextAlignment.JUSTIFY);
            textTime.setStyle("-fx-font-weight: bold");

            Group menuItem = new Group();
            menuItem.getChildren().addAll(rect, textPlayer, textScore, textTime);

            root.getChildren().addAll(menuItem);
        }
    }

    public LevelCompleteTopList(int index) {
        this(index, MENU_TEXTS, Main.WINDOW_HEIGHT * 0.8);
    }

    @Override
    public void menuItemAction()
    {
        if(selectedIndex == START_MENU)
        {
            Main.instance.toStartMenu();
        }
    }
}
