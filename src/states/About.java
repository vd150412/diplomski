package states;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import main.Main;

public class About extends GameMenu implements StateController{

    private static final String[] MENU_TEXTS = {
            "OK"
    };

    private static final String[] ABOUT_TEXTS = {
            "Borba svetlosti i tame v1.0",
            "Diplomski rad osnovnih akademskih studija",
            "",
            "Opis: ",
            "Cilj igre je eliminisati sve kugle",
            "pre nego što one stignu do meseca",
            "",
            "Autor: Danilo Vučinić",
            "Mentor: prof. dr. Igor Tartalja",
            "",
            "© Univerzitet u Beogradu",
            "Elektrotehnički fakultet, 2019"
    };

    protected static final Font FONT_ABOUT = Font.font("Arial",  FontWeight.EXTRA_BOLD, 20);

    public About() {
        super("ABOUT", MENU_TEXTS, Color.color(0, 0, 0, 0.65), Main.WINDOW_HEIGHT * 0.8);

        final double RECT_CENTER_WIDTH = Main.WINDOW_WIDTH / 2.5;
        Rectangle background = new Rectangle(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2, -50, RECT_CENTER_WIDTH, Main.WINDOW_HEIGHT + 50*2);
        background.setFill( Color.color(0, 0, 0, 1));
        root.getChildren().add(1, background);

        for(int i = 0; i < ABOUT_TEXTS.length; i++)
        {
            Text text = new Text(ABOUT_TEXTS[i]);
            text.setFont(FONT_ABOUT);
            text.setFill(Color.WHITE);
            text.setX(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2 + 30);
            text.setY(Main.WINDOW_HEIGHT * 0.28 + i * 25);
            text.setTextAlignment(TextAlignment.JUSTIFY);
            root.getChildren().addAll(text);
        }
    }

    @Override
    public void menuItemAction() {
        Main.instance.fromAbout();
    }

    @Override
    public void onKeyPressed(KeyEvent event)
    {
        super.onKeyPressed(event);

        if(event.getCode() == KeyCode.ESCAPE)
        {
            Main.instance.toStartMenu();
        }
    }
}
