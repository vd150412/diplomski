package states;

import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import main.Main;

public class GameOver extends GameMenu implements StateController{

    private static final String[] MENU_TEXTS = {
            "RELOAD",
            "START MENU",
            "EXIT"
    };

    private static final int RELOAD = 0;
    private static final int START_MENU = 1;
    private static final int EXIT = 2;

    public GameOver()
    {
        super("GAME OVER!", MENU_TEXTS, Color.color(0, 0, 0, 0.65));

        final double rectW = Main.WINDOW_WIDTH / 2.5;
        Rectangle background = new Rectangle(Main.WINDOW_WIDTH / 2 - rectW/2, 0, rectW, Main.WINDOW_HEIGHT + 10);
        background.setFill( Color.color(0, 0, 0, 0.65));
        root.getChildren().add(1, background);
    }

    @Override
    public void menuItemAction()
    {
        if(selectedIndex == RELOAD)
        {
            Main.instance.reloadGame();
        }
        else if(selectedIndex == START_MENU)
        {
            Main.instance.toStartMenu();
        }
        else if(selectedIndex == EXIT)
        {
            Platform.exit();
        }
    }
}
