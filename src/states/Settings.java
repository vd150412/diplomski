package states;

import controllers.Config;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import main.Main;
import objects.UI.BackIcon;
import objects.UI.GameIcon;
import java.io.File;


public class Settings extends GameMenu implements StateController {

    private static final Stop[] stops = new Stop []{
            new Stop(0, Color.LIGHTSEAGREEN),
            new Stop(0.8, Color.LIGHTBLUE),
            new Stop(1, Color.LIGHTSEAGREEN)
    };

    private static final LinearGradient BACKGROUND_FILL = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);

    private static final String[] MENU_TEXTS = {
            "FULL SCREEN",
            "VIEW CONFIG",
            "LOAD CONFIG FILE"
    };

    private static final int VIEW_CONFIG = 1;
    private static final int LOAD_CONFIG_FILE = 2;
    private static final int FULLSCREEN = 0;

    private CheckBox isFullScreen;

    public Settings()
    {
        super("SETTINGS", MENU_TEXTS, BACKGROUND_FILL, Main.WINDOW_HEIGHT * 0.26);

        Paint backIconFill = Color.color(0, 0,0 , 0.4);
        BackIcon backIcon = new BackIcon(Main.WINDOW_WIDTH - GameIcon.ICON_RADIUS * 2, Main.WINDOW_HEIGHT - GameIcon.ICON_RADIUS * 2, backIconFill, e ->{
            Main.instance.toStartMenu();
        });

        Text fullScreenText =  (Text) (((Group)(rects[FULLSCREEN].getParent())).getChildren().get(1));
        fullScreenText.setTranslateX(fullScreenText.getTranslateX() + 25);
        isFullScreen = new CheckBox();
        double y = Main.WINDOW_HEIGHT * 0.26 +  FULLSCREEN * MENU_ITEM_DISTANCE + FULLSCREEN * RECT_HEIGHT + RECT_HEIGHT / 4;
        double x = Main.WINDOW_WIDTH / 2 - RECT_WIDTH / 2 + 40;
        isFullScreen.setTranslateX(x);
        isFullScreen.setTranslateY(y);
        isFullScreen.setSelected(true);
        isFullScreen.setStyle(
            "-fx-font-size: 20;"
        );

        isFullScreen.selectedProperty().addListener((ov, old_val, new_val) -> {
            Main.instance.primaryStage.setFullScreen(new_val);
        });

        root.getChildren().addAll(backIcon, isFullScreen);
    }

    public void activate() {
        isFullScreen.setSelected(Main.instance.primaryStage.isFullScreen());
    }

    @Override
    public void onKeyPressed(KeyEvent event)
    {
        super.onKeyPressed(event);

        if(event.getCode() == KeyCode.ESCAPE)
        {
            Main.instance.toStartMenu();
        }
    }

    @Override
    public void menuItemAction()
    {
        if(selectedIndex == VIEW_CONFIG)
        {
            Main.instance.toConfig();
        }
        else if(selectedIndex == LOAD_CONFIG_FILE)
        {
            loadConfigFile();
        }
    }

    private void loadConfigFile()
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(Config.getFileChooserDir());
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle("Load Config File");
        File selectedFile = fileChooser.showOpenDialog(Main.instance.primaryStage);
        if(selectedFile != null && !selectedFile.isDirectory())
        {
            Config.FILE_CHOOSER_DIR = selectedFile.getParentFile();
        }

        if(selectedFile == null) return;

        boolean error = Config.LOAD_JSON(selectedFile);

        if(error)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Settings");
            alert.setHeaderText(null);
            alert.setContentText("Error loading JSON file");
            alert.showAndWait();
        }
        else
        {
            Main.instance.toConfig();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Settings");
            alert.setHeaderText(null);
            alert.setContentText("JSON file is successfully loaded");
            alert.showAndWait();
        }
    }

}
