package states;

import controllers.Config;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import main.Main;

import java.util.logging.Level;

public class LevelComplete extends GameMenu implements StateController{

    protected static final Font INFO_FONT = Font.font("Courier New", 40);
    private static final String[] MENU_TEXTS = {
            "RELOAD",
            "START MENU",
            "EXIT"
    };

    private static final int RELOAD = 0;
    private static final int START_MENU = 1;
    private static final int EXIT = 2;

    protected Text textScore;
    protected Text textTime;

    protected double RECT_CENTER_WIDTH = Main.WINDOW_WIDTH / 2.5;

    public LevelComplete(String[] texts, double margin)
    {
        super("LEVEL COMPLETE!", texts, Color.color(0, 0, 0, 0.65), margin);


        Rectangle background = new Rectangle(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2, 0, RECT_CENTER_WIDTH, Main.WINDOW_HEIGHT + 10);
        background.setFill( Color.color(0, 0, 0, 0.65));
        root.getChildren().add(1, background);

        textScore = new Text(Main.WINDOW_WIDTH / 2, Main.WINDOW_HEIGHT * 0.30, "");
        textScore.setFont(INFO_FONT);
        textScore.setFill(Color.WHITE);
        textScore.setX(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2 + 50);
        textScore.setTextAlignment(TextAlignment.JUSTIFY);
        textScore.setStyle("-fx-font-weight: bold");
        root.getChildren().add(2, textScore);

        textTime = new Text(Main.WINDOW_WIDTH / 2, Main.WINDOW_HEIGHT * 0.40, "Time:  " + Main.instance.game.textTime.getTime() + " s");
        textTime.setFont(INFO_FONT);
        textTime.setFill(Color.WHITE);
        textTime.setX(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2 + 50);
        textTime.setTextAlignment(TextAlignment.JUSTIFY);
        textTime.setStyle("-fx-font-weight: bold");
        root.getChildren().add(2, textTime);
    }

    public LevelComplete() {
        this(MENU_TEXTS, Main.WINDOW_HEIGHT / 2);
    }

    public void onLevelComplete() {
        textScore.setText("Score: " + Config.FINAL_SCORE());
        textTime.setText("Time:  " + Main.instance.game.textTime.getTime() + " s");
    }

    @Override
    public void menuItemAction()
    {
        if(selectedIndex == RELOAD)
        {
            Main.instance.reloadGame();
        }
        else if(selectedIndex == START_MENU)
        {
            Main.instance.toStartMenu();
        }
        else if(selectedIndex == EXIT)
        {
            Platform.exit();
        }
    }
}
