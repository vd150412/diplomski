package states;

import controllers.Config;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import main.Main;

public class LevelCompleteEnterName extends LevelComplete implements StateController {

    protected static final Font FONT_ENTER_YOUR_NAME = Font.font("Courier New", 25);
    private static final String[] MENU_TEXTS = {
            "VIEW TOP LIST"
    };

    private static final int VIEW_TOP_LIST = 0;

    private TextField textField;

    public LevelCompleteEnterName()
    {
        super(MENU_TEXTS, Main.WINDOW_HEIGHT * 0.8);

        textField = new TextField();
        textField.setFont(INFO_FONT);
        textField.setTranslateX(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2 + 50);
        textField.setTranslateY(Main.WINDOW_HEIGHT * 0.6);
        textField.setPrefWidth(RECT_CENTER_WIDTH - 100);
        textField.setId("textField");
        textField.setPrefColumnCount(15);
        textField.setPromptText("ENTER YOUR NAME");

        Text text = new Text("Enter your name: ");
        text.setFont(FONT_ENTER_YOUR_NAME);
        text.setFill(Color.WHITE);
        text.setX(Main.WINDOW_WIDTH / 2 - RECT_CENTER_WIDTH/2 + 50);
        text.setY(Main.WINDOW_HEIGHT * 0.57);
        text.setTextAlignment(TextAlignment.JUSTIFY);
        text.setStyle("-fx-font-weight: bold");

        root.getChildren().addAll(textField, text);
    }

    @Override
    public void onKeyPressed(KeyEvent event) {
        if(event.getCode() == KeyCode.ENTER && textField.getText().isEmpty() == false)
        {
            menuItemAction();
        }
    }

    @Override
    public void menuItemAction()
    {
        if(selectedIndex == VIEW_TOP_LIST)
        {
            int finalScore = Config.FINAL_SCORE();
            int index = Main.instance.topListController.onNameEntered(textField.getText(), finalScore, Main.instance.game.textTime.getTime());
            Main.instance.topListLevelComplete(index);
        }
    }
}
