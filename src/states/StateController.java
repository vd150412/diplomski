package states;

import javafx.scene.Group;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public interface StateController {
    void update();
    void onMouseMoved(MouseEvent event);
    void onMouseClicked(MouseEvent event);
    void onKeyPressed(KeyEvent event);
    default void onKeyReleased(KeyEvent event) {

    }
    Group getRoot();
}
