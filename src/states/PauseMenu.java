package states;

import javafx.application.Platform;
import javafx.scene.paint.Color;
import main.Main;

public class PauseMenu extends GameMenu implements StateController{

    private static final String[] MENU_TEXTS = {
            "RESUME",
            "PLAY AGAIN",
            "START MENU"
    };

    private static final int RESUME = 0;
    private static final int PLAY_AGAIN = 1;
    private static final int START_MENU = 2;

    public PauseMenu()
    {
        super("", MENU_TEXTS, Color.color(0, 0, 0, 0.65));
    }

    @Override
    public void menuItemAction()
    {
        if(selectedIndex == RESUME)
        {
            Main.instance.resumeGame();
        }
        else if(selectedIndex == PLAY_AGAIN)
        {
            Main.instance.reloadGame();
        }
        else if(selectedIndex == START_MENU)
        {
            Main.instance.toStartMenu();
        }
    }
}