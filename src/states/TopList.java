package states;

import controllers.TopListController;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Pair;
import main.Main;
import objects.UI.BackIcon;
import objects.UI.GameIcon;

import java.util.ArrayList;

public class TopList implements StateController {

    private static final double RECT_WIDTH = Main.WINDOW_WIDTH / 2;
    private static final double RECT_HEIGHT = 50;

    private static final double MENU_ITEM_DISTANCE = 5;
    private static final double MENU_ITEM_ARC = 10;
    private static final Font MENU_ITEM_FONT = Font.font("Courier New", 30);
    private static final Font TOP_LIST_TITLE_FONT = Font.font("Courier New", 70);

    protected Group root;

    private static final Stop[] stops = new Stop []{
            new Stop(0.0, Color.LIGHTBLUE),
            new Stop(0.3, Color.LIGHTSEAGREEN),
            new Stop(0.7, Color.LIGHTSEAGREEN),
            new Stop(1.0, Color.LIGHTBLUE)
    };

    private static final LinearGradient BACKGROUND_FILL = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);

    public TopList()
    {
        root = new Group();
        Rectangle background = new Rectangle(0, 0, Main.WINDOW_WIDTH + 10, Main.WINDOW_HEIGHT + 10);
        background.setFill(BACKGROUND_FILL);

        root.getChildren().add(background);

        Text textTopList = new Text(Main.WINDOW_WIDTH / 2, Main.WINDOW_HEIGHT * 0.1, "TOP LIST");
        textTopList.setFont(TOP_LIST_TITLE_FONT);
        textTopList.setFill(Color.WHITE);
        textTopList.setX(Main.WINDOW_WIDTH / 2 - textTopList.getBoundsInParent().getWidth() / 2);
        textTopList.setTextAlignment(TextAlignment.JUSTIFY);
        textTopList.setStyle("-fx-font-weight: bold");
        root.getChildren().addAll(textTopList);

        ArrayList<TopListController.TopListEntry> topList = Main.instance.topListController.getTopList();
        int numOfMenuItems = topList.size();
        double margin = (Main.WINDOW_HEIGHT - MENU_ITEM_DISTANCE * 9 - RECT_HEIGHT * 10) / 1.4;
        double rectX = Main.WINDOW_WIDTH / 2 - RECT_WIDTH / 2;
        double playerX = rectX + 25;
        double scoreX = rectX + RECT_WIDTH * 0.65;
        double timeX = rectX + RECT_WIDTH * 0.85;

        for(int i = 0; i < 10; i++)
        {
            String playerStr = i < numOfMenuItems ? topList.get(i).name  : "";
            String scoreStr = i < numOfMenuItems ? topList.get(i).score + "" : "";
            String timeStr = i < numOfMenuItems ? topList.get(i).time + "s" : "";

            double rectY = margin + i * MENU_ITEM_DISTANCE + i * RECT_HEIGHT;

            Rectangle rect = new Rectangle(rectX, rectY, RECT_WIDTH, RECT_HEIGHT);
            final double gray = 0.35;
            rect.setFill(Color.color(gray, gray, gray));
            rect.setStroke(Color.WHITE);
            rect.setStrokeWidth(5);
            rect.setArcHeight(MENU_ITEM_ARC);
            rect.setArcWidth(MENU_ITEM_ARC);

            Text textPlayer = new Text(playerX, rectY, playerStr);
            textPlayer.setFont(MENU_ITEM_FONT);
            textPlayer.setFill(Color.WHITE);
            textPlayer.setY(rectY + RECT_HEIGHT / 2 + textPlayer.getBoundsInParent().getHeight() / 4);
            textPlayer.setTextAlignment(TextAlignment.JUSTIFY);
            textPlayer.setStyle("-fx-font-weight: bold");

            Text textScore = new Text(scoreX, rectY, "" + scoreStr);
            textScore.setFont(MENU_ITEM_FONT);
            textScore.setFill(Color.WHITE);
            textScore.setY(rectY + RECT_HEIGHT / 2 + textScore.getBoundsInParent().getHeight() / 4);
            textScore.setTextAlignment(TextAlignment.JUSTIFY);
            textScore.setStyle("-fx-font-weight: bold");

            Text textTime = new Text(timeX, rectY, timeStr);
            textTime.setFont(MENU_ITEM_FONT);
            textTime.setFill(Color.WHITE);
            textTime.setY(rectY + RECT_HEIGHT / 2 + textScore.getBoundsInParent().getHeight() / 4);
            textTime.setTextAlignment(TextAlignment.JUSTIFY);
            textTime.setStyle("-fx-font-weight: bold");

            Group menuItem = new Group();
            menuItem.getChildren().addAll(rect, textPlayer, textScore, textTime);

            root.getChildren().addAll(menuItem);
        }

        Paint backIconFill = Color.color(0, 0,0 , 0.4);
        BackIcon backIcon = new BackIcon(Main.WINDOW_WIDTH - GameIcon.ICON_RADIUS * 2, Main.WINDOW_HEIGHT - GameIcon.ICON_RADIUS * 2, backIconFill, e ->{
            Main.instance.fromTopList();
        });
        root.getChildren().addAll(backIcon);
    }

    @Override
    public void update() {

    }

    @Override
    public void onMouseMoved(MouseEvent event) {

    }

    @Override
    public void onMouseClicked(MouseEvent event) {

    }

    @Override
    public void onKeyPressed(KeyEvent event) {
        if(event.getCode() == KeyCode.ESCAPE) {
            Main.instance.fromTopList();
        }
    }

    @Override
    public Group getRoot() {
        return root;
    }
}