package main;

import controllers.Config;
import controllers.TopListController;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;
import javafx.util.Pair;
import states.*;
import javafx.geometry.Rectangle2D;
import javafx.scene.transform.Scale;
import javafx.stage.Screen;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.*;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.zip.DeflaterOutputStream;

public class Main extends Application implements EventHandler<Event> {

    public static double WINDOW_WIDTH = 1200;
    public static double WINDOW_HEIGHT = 700;

    public static double SCREEN_WIDTH;
    public static double SCREEN_HEIGHT;

    public static double DEVICE_WIDTH;
    public static double DEVICE_HEIGHT;

    public GameState gameState = GameState.GAME;
    public static Main instance;
    public Group rootMain;
    public Stage primaryStage;
    public TopListController topListController;

    public StateController currentState;
    public Game game;
    public StartMenu startMenu;
    public PauseMenu pauseMenu;
    public TopList topList;
    public GameOver gameOver;
    public LevelComplete levelComplete;
    public LevelCompleteEnterName levelCompleteEnterName;
    public LevelCompleteTopList levelCompleteTopList;
    public Settings settings;
    public CreateLevel createLevel;
    public About about;
    public ConfigScene configScene;

    @Override
    public void start(Stage primaryStage)
    {
        instance = this;
        Config.LOAD_JSON(new File(Config.CONFIG_FILE_PATH));
        this.primaryStage = primaryStage;
        topListController = new TopListController();

        game = new Game();
        startMenu = new StartMenu();
        pauseMenu = new PauseMenu();
        gameOver = new GameOver();
        levelComplete = new LevelComplete();
        levelCompleteEnterName = new LevelCompleteEnterName();
        settings = new Settings();
        createLevel = new CreateLevel();
        currentState = startMenu;

        rootMain = new Group();
        Scene sceneMain = new Scene(rootMain, WINDOW_WIDTH, WINDOW_HEIGHT);
        rootMain.getChildren().add(currentState.getRoot());

        sceneMain.setOnMouseClicked(this);
        sceneMain.setOnMouseMoved(this);
        sceneMain.setOnKeyPressed(this);
        sceneMain.setOnKeyReleased(this);

        scaleScreen(sceneMain, primaryStage);
        primaryStage.setTitle("Borba svetlosti");
        primaryStage.setScene(sceneMain);
        primaryStage.setResizable(false);
        //primaryStage.setFullScreen(true);
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.show();
        //Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        //primaryStage.setX(primScreenBounds.getWidth() + (primScreenBounds.getWidth() - primaryStage.getWidth()) / 2);
        //primaryStage.setY((primScreenBounds.getHeight() - primaryStage.getHeight()) / 2);

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) 
            {
                if(!primaryStage.isFocused()) return;
                currentState.update();
            }
        };
        timer.start();
    }

    public void changeState(StateController newState)
    {
        //rootMain.getChildren().removeAll(currentState.getRoot());
        rootMain.getChildren().addAll(newState.getRoot());
        currentState = newState;
    }

    public void resumeGame() {
        rootMain.getChildren().removeAll(currentState.getRoot());
        currentState = game;
    }

    public void toStartMenu() {
        rootMain.getChildren().removeAll(currentState.getRoot(), game.getRoot());
        game = new Game();
        currentState = startMenu;
        gameState = GameState.GAME;
    }

    public void toTopList() {
        topList = new TopList();
        rootMain.getChildren().add(topList.getRoot());
        currentState = topList;
    }

    public void fromTopList() {
        rootMain.getChildren().remove(currentState.getRoot());
        currentState = startMenu;
    }

    //called from moon
    public void onGameOver() {
        rootMain.getChildren().addAll(gameOver.getRoot());
        currentState = gameOver;
    }

    public void reloadGame() {
        rootMain.getChildren().removeAll(currentState.getRoot(), game.getRoot());

        if(game.isCreatedLevel)
        {
            ArrayList<Line> pairs = createLevel.getLines();
            Pair<Double, Double> sunPos = createLevel.getSunPosition();
            game = new Game(true, pairs, sunPos);
        }
        else
        {
            game = new Game();
        }

        rootMain.getChildren().addAll(game.getRoot());
        currentState = game;
        gameState = GameState.GAME;
    }

    public void levelComplete() {
        boolean top10 = topListController.isInTop10();
        if(top10 == false || game.isCreatedLevel)
        {
            rootMain.getChildren().addAll(levelComplete.getRoot());
            levelComplete.onLevelComplete();
            currentState = levelComplete;
        }
        else
        {
            rootMain.getChildren().addAll(levelCompleteEnterName.getRoot());
            levelCompleteEnterName.onLevelComplete();
            currentState = levelCompleteEnterName;
        }
    }

    public void topListLevelComplete(int index) {
        levelCompleteTopList = new LevelCompleteTopList(index);
        rootMain.getChildren().removeAll(levelCompleteEnterName.getRoot());
        rootMain.getChildren().addAll(levelCompleteTopList.getRoot());
        currentState = levelCompleteTopList;
    }

    public void toSettings() {
        rootMain.getChildren().addAll(settings.getRoot());
        settings.activate();
        currentState = settings;
    }

    public void toCreateLevel() {
        rootMain.getChildren().addAll(createLevel.getRoot());
        currentState = createLevel;
    }

    public void toGameFromCreateLevel() {
        ArrayList<Line> linesCreateLevel = createLevel.getLines();
        Pair<Double, Double> sunCreateLevel = createLevel.getSunPosition();

        String pathStr = Config.getPathStr(linesCreateLevel);
        String sunStr = Config.getSunStr(createLevel.sun);

        if(Config.LEVEL_PATH.equals(pathStr) && sunStr.equals(Config.LEVEL_SUN_POSITION))
        {
            game = new Game();
            System.out.println("CREATE LEVEL TOURNAMENT!");
        }
        else
        {
            System.out.println("CREATE LEVEL DEMO");
            game = new Game(true, linesCreateLevel, createLevel.getSunPosition());
        }

        rootMain.getChildren().removeAll(currentState.getRoot());
        rootMain.getChildren().addAll(game.getRoot());
        currentState = game;
    }

    public void toAbout() {
        about = new About();
        rootMain.getChildren().addAll(about.getRoot());
        currentState = about;
    }

    public void fromAbout() {
        rootMain.getChildren().removeAll(about.getRoot());
        currentState = startMenu;
    }
    public void toConfig() {
        configScene = new ConfigScene();
        rootMain.getChildren().addAll(configScene.getRoot());
        currentState = configScene;
    }

    public void fromConfig() {
        rootMain.getChildren().removeAll(configScene.getRoot());
        settings.activate();
        currentState = settings;
    }

    private void scaleScreen(Scene scene, Stage stage)
    {
        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        DEVICE_WIDTH = bounds.getWidth();
        DEVICE_HEIGHT = bounds.getHeight() + 40;

        stage.widthProperty().addListener((obs, oldVal, newVal) -> {

            double widthNew = (double) newVal;
            if(widthNew != DEVICE_WIDTH) SCREEN_WIDTH = widthNew - 16;
            else SCREEN_WIDTH = widthNew;
            setScale(scene);
        });

        stage.heightProperty().addListener((obs, oldVal, newVal) -> {
            double heightNew = (double) newVal;
            if(heightNew != DEVICE_HEIGHT) SCREEN_HEIGHT = heightNew - 39;
            else SCREEN_HEIGHT = heightNew;
            setScale(scene);
        });
    }

    private Scale oldScale;
    private void setScale(Scene scene) {
        Scale scale = new Scale(SCREEN_WIDTH / WINDOW_WIDTH, SCREEN_HEIGHT / WINDOW_HEIGHT);
        scale.setPivotX(0);
        scale.setPivotY(0);
        if(oldScale != null) scene.getRoot().getTransforms().removeAll(oldScale);
        scene.getRoot().getTransforms().addAll(scale);
        oldScale = scale;
    }
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(Event event)
    {
        if(event instanceof  MouseEvent)
        {
            if(event.getEventType() == MouseEvent.MOUSE_MOVED)
            {
                currentState.onMouseMoved((MouseEvent)event);
            }
            else if(event.getEventType() == MouseEvent.MOUSE_CLICKED)
            {
                currentState.onMouseClicked((MouseEvent)event);
            }
        }
        else if(event instanceof KeyEvent)
        {
            if(event.getEventType() == KeyEvent.KEY_PRESSED)
            {
                currentState.onKeyPressed((KeyEvent)event);
            }
            else if(event.getEventType() == KeyEvent.KEY_RELEASED)
            {
                currentState.onKeyReleased((KeyEvent)event);
            }
        }
    }
}
